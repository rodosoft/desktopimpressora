﻿namespace Impressoras.Extensions
{
    public static class ByteExtensions
    {
        public static bool GetBit(this byte bit, int pos) => (bit & (1 << pos)) != 0;
    }
}
