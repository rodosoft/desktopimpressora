﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Impressoras.Extensions
{
    public static class EnumExtension
    {
        public static string GetDescription(this ValueType value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fieldInfo.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
    }
}
