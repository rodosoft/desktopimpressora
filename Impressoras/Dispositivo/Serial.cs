﻿using Impressoras.Enum;
using System;
using System.IO.Ports;

namespace Impressoras.Dispositivo
{
    public class Serial
    {
        public bool SerialOK = false;
        public double Peso;
        private SerialPort serial;
        ImprimirPassagem _imprimirPassagem;
        public string retornoSerial;
        public string RetornoSerial
        {
            get { return retornoSerial; }
            set { retornoSerial = value; }
        }

        public bool isOpen
        {
            get { return serial.IsOpen; }
        }

        public string porta
        {
            get { return serial.PortName; }
        }

        public int readTimeout
        {
            get { return serial.ReadTimeout; }
            set { serial.ReadTimeout = value; }
        }

        public int writeTimeout
        {
            get { return serial.WriteTimeout; }
            set { serial.WriteTimeout = value; }
        }

        private TipoQualidade qualidade = TipoQualidade.NAO_OK;
        public TipoQualidade Qualidade
        {
            get { return qualidade; }
        }

        public SerialDataReceivedEventHandler DataReceivedHandler { private get; set; }

        public SerialPort SerialPort => serial;
        public Serial(string Com,
                     int BaudRate,
                     Parity parity,
                     int DataBits,
                     int stopBits,
                     int Read,
                     int Write,
                     ImprimirPassagem imprimir)
        {
            try
            {
                serial = new SerialPort();
                serial.PortName = Com;
                serial.BaudRate = BaudRate;
                serial.Parity = parity;
                serial.DataBits = DataBits;
                if (stopBits == 1) serial.StopBits = StopBits.One;
                if (stopBits == 1.5) serial.StopBits = StopBits.OnePointFive;
                if (stopBits == 2) serial.StopBits = StopBits.Two;
                if (stopBits == 0) serial.StopBits = StopBits.None;
                serial.Handshake = Handshake.None;
                serial.RtsEnable = true;
                serial.ReadTimeout = Read;
                serial.WriteTimeout = Write;
                //serial.Handshake = SetPortHandshake(serial.Handshake);
                serial.DataReceived += DataReceivedHandler;
                serial.Open();
                this.SerialOK = true;
                this.retornoSerial = string.Empty;
                _imprimirPassagem = imprimir;
            }
            catch (Exception)
            {
                if (serial != null)
                    serial.Close();
            }
        }
        public Serial(string Com,
                    int BaudRate,                    
                    int DataBits,
                    int stopBits,
                    int Read,
                    int Write)
        {
            try
            {
                serial = new SerialPort();
                serial.PortName = Com;
                serial.BaudRate = BaudRate;
                serial.Parity = Parity.None;
                serial.DataBits = DataBits;
                if (stopBits == 1) serial.StopBits = StopBits.One;
                if (stopBits == 1.5) serial.StopBits = StopBits.OnePointFive;
                if (stopBits == 2) serial.StopBits = StopBits.Two;
                if (stopBits == 0) serial.StopBits = StopBits.None;
                serial.ReadTimeout = Read;
                serial.WriteTimeout = Write;
                //serial.Handshake = SetPortHandshake(serial.Handshake);
               
                serial.Open();
                this.SerialOK = true;
                this.retornoSerial = string.Empty;
           }
            catch (Exception)
            {
                if (serial != null)
                    serial.Close();
            }
        }
        ~Serial()
        {
            if (serial != null)
                serial.Close();
        }
        public static Handshake SetPortHandshake(Handshake defaultPortHandshake)
        {
            string handshake;
                       
            Console.Write("Enter Handshake value (Default: {0}):", defaultPortHandshake.ToString());
            handshake = Console.ReadLine();


            handshake = defaultPortHandshake.ToString();
            
            return (Handshake)System.Enum.Parse(typeof(Handshake), handshake, true);
        }
        
        
        public bool EnviaDados(byte[] bufSaida)
        {
            if (this.SerialOK)
            {
                try
                {
                    this.serial.DiscardOutBuffer();
                    serial.Write(bufSaida, 0, bufSaida.Length);
                    return true;
                }
                catch (Exception)
                {
                }
            }
            return false;
        }

        public void LimpaBufferEntrada()
        {
            this.serial.DiscardInBuffer();
            this.serial.DiscardOutBuffer();
        }

        public void EncerraSerial()
        {
            serial.Close();
        }

        public ImprimirPassagem ImprimirPassagem => _imprimirPassagem;
    }
}
