﻿using Impressoras.Extensions;
using System;
using System.IO.Ports;

namespace Impressoras.Dispositivo
{
    public class Elgin : ImprimirPassagem
    {
        public Elgin(string porta, int baudRate)
        {
            serial = new Serial(porta, baudRate == 0 ? 38400 : baudRate, Parity.None, 8, 1, 130, 130, this);
            serial.SerialPort.DataReceived += new SerialDataReceivedEventHandler(onRetornoSerial);
        }         

        public override string LeDadosPrinterOnOff()
        {
            try
            {
                serial.EnviaDados(Clear_Buffer());
                //Ativa retorno de status automatico (ABS)
                serial.EnviaDados(Automatic_Back_Status_ON());

                //Verifica os status
                string retorno = string.Empty;
                serial.EnviaDados(Real_time_status_transmission(5));
                //Lê a porta e se ocorrer timeout está desligada
                byte b = (byte)serial.SerialPort.ReadByte();

                bool bitFixo = b.GetBit(6);
                if (!bitFixo)   //Esse bit deve estar fixado em 0 para real time 5
                {
                    if (b.GetBit(0))
                        retorno = "2-Pouco Papel|";
                    if (b.GetBit(1))
                        retorno += "1-Tampa Aberta|";
                    if (b.GetBit(2))
                        retorno += "3-Sem Papel|";
                }

                #region Atolamento de Papel
                serial.EnviaDados(Real_time_status_transmission(6));
                b = (byte)serial.SerialPort.ReadByte();

                bitFixo = b.GetBit(7);
                if (!bitFixo)   //Esse bit deve estar fixado em 0 para real time 6
                {
                    if (b.GetBit(1) || b.GetBit(2) || b.GetBit(4))
                        retorno += "4-Papel Atolado";
                }

                //Qnd obstruem a saida do papel com o dedo, ocorre um atolamento que só aparece com este comando
                if (!retorno.Contains("4"))
                {
                    serial.EnviaDados(Real_time_status_transmission(7));
                    b = (byte)serial.SerialPort.ReadByte();
                    if (b == 22)
                        retorno += "4-Papel Atolado";
                }
                #endregion

                return retorno;
            }
            catch (Exception)
            {
                serial.EnviaDados(Clear_Buffer());
                return "0-Impressora Desligada ou Inoperante";
            }
        }

        protected override void VerificaRetornoSerial(SerialPort serialAtual)
        {
            try
            {
                //Recebe o retorno da Serial
                byte[] bytesRetorno = new byte[8];
                serialAtual.Read(bytesRetorno, 0, 8);

                //Limpa o retorno
                serial.RetornoSerial = string.Empty;

                //Valida o Bit3 do Byte1
                if (bytesRetorno[0].GetBit(2))
                    serial.RetornoSerial += "Sem Papel | ";

                //Valida o Bit3 do Byte2
                if (bytesRetorno[1].GetBit(2))
                    serial.RetornoSerial += "Papel Atolado | ";

                //Qnd obstruem a saida do papel com o dedo, ocorre um atolamento que só aparece no Byte3
                if (!serial.RetornoSerial.Contains("Atolado") && bytesRetorno[2] == 22)
                    serial.RetornoSerial += "Papel Atolado | ";
            }
            catch (Exception)
            {

            }
        }
    }
}
