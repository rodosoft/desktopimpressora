﻿
using System;
using System.Globalization;
using System.IO.Ports;
using System.Text;

namespace Impressoras.Dispositivo
{
    public abstract  class ImprimirPassagem
    {
        protected Serial serial;
        #region Layout Impressão
        //=====================================================//
        // CENTRALIZADO COM FONTE PEQUENA                      // 
        //=====================================================//
        public virtual void Central_Aligment_Small_Source_Small_Normal(string Com, Serial serial)
        {
            byte[] bytestosend;
            bytestosend = SelectCharacterPrintModes(01);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_character_alignment_mode(01);
            serial.EnviaDados(bytestosend);
            bytestosend = Turn_emphasized_mode_onoff(00);
            serial.EnviaDados(bytestosend);
            bytestosend = Set_line_spacing(22);
            serial.EnviaDados(bytestosend);
            bytestosend = Set_line_spacing(22);
            serial.EnviaDados(bytestosend);
        }
        //=====================================================//
        // CENTRALIZADO COM FONTE PEQUENA E NEGRITO            // 
        //=====================================================//
        public virtual void Central_Aligment_Small_Source_Small_Bold(string Com, Serial serial)
        {
            byte[] bytestosend;
            bytestosend = SelectCharacterPrintModes(01);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_character_alignment_mode(01);
            serial.EnviaDados(bytestosend);
            bytestosend = Turn_emphasized_mode_onoff(01);
            serial.EnviaDados(bytestosend);
            serial.EnviaDados(bytestosend);
            bytestosend = Set_line_spacing(22);
        }
        //=======================================================//
        //              lAYOUT CODIGO BARRA 128                  //
        //=======================================================//
        public virtual void Central_Alignment_Large_Font_Normal_Font(string Com, Serial serial)
        {
            byte[] bytestosend;
            bytestosend = SelectCharacterPrintModes(00);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_character_alignment_mode(01);
            serial.EnviaDados(bytestosend);
            bytestosend = Turn_emphasized_mode_onoff(00);
            serial.EnviaDados(bytestosend);
        }


        //=====================================================//
        // Alinhamento Central Fonte Grande Fonte Normal       // 
        //=====================================================//
        public virtual void Layout_Cod_Bar_128(string Com, Serial serial)
        {
            byte[] bytestosend;
            bytestosend = Select_font_Human_Readable_Interpretation_HRI_characters(01);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_barcode_character_condensed();
            serial.EnviaDados(bytestosend);
            bytestosend = Barcode_Character_Above();
            serial.EnviaDados(bytestosend);
            bytestosend = Select_barcode_height(80);
            serial.EnviaDados(bytestosend);
        }
        //==========================================================//
        // Alinhamento Central Fonte Grande Fonte Normal Negrito    // 
        //=========================================================//
        public void Central_Alignment_Large_Font_Normal_Bold(string Com, Serial serial)
        {
            byte[] bytestosend;
            bytestosend = SelectCharacterPrintModes(00);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_character_alignment_mode(01);
            serial.EnviaDados(bytestosend);
            bytestosend = Turn_emphasized_mode_onoff(01);
            serial.EnviaDados(bytestosend);
        }

        //=======================================================================//
        // Alinhamento central Fonte grande Tamanho da fonte da fonte normal     //
        //=======================================================================//
        public virtual void Central_Alignment_Large_Font_Normal_Font_Font_Size(string Com, Serial serial, int SizeFont)
        {
            byte[] bytestosend;
            bytestosend = SelectCharacterPrintModes(00);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_character_alignment_mode(01);
            serial.EnviaDados(bytestosend);
            bytestosend = Turn_emphasized_mode_onoff(00);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_character_size(SizeFont);
            serial.EnviaDados(bytestosend);

        }


        //================================================================================//
        // Alinhamento central Fonte grande Tamanho da fonte da fonte normal Negrito      //
        //================================================================================//
        public virtual void Central_Alignment_Large_Font_Normal_Font_Font_Size_Bold(string Com, Serial serial, int SizeFont)
        {
            byte[] bytestosend;
            bytestosend = SelectCharacterPrintModes(00);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_character_alignment_mode(01);
            serial.EnviaDados(bytestosend);
            bytestosend = Turn_emphasized_mode_onoff(01);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_character_size(SizeFont);
            serial.EnviaDados(bytestosend);

        }
        //======================================================================//
        // Alinhamento à Esquerda Fonte Grande Fonte Normal                     //
        //======================================================================//
        public virtual void Left_Alignment_Large_Font_Normal_Font(string Com, Serial serial)
        {
            byte[] bytestosend;
            bytestosend = SelectCharacterPrintModes(00);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_character_alignment_mode(00);
            serial.EnviaDados(bytestosend);
            bytestosend = Turn_emphasized_mode_onoff(00);
            serial.EnviaDados(bytestosend);
        }
        //======================================================================//
        // Alinhamento Esquerdo Fonte Grande Fonte Normal                       //
        //======================================================================//
        public virtual void Right_Alignment_Large_Font_Normal_Font(string Com, Serial serial)
        {
            byte[] bytestosend;
            bytestosend = SelectCharacterPrintModes(02);
            serial.EnviaDados(bytestosend);
            bytestosend = Select_character_alignment_mode(02);
            serial.EnviaDados(bytestosend);
            bytestosend = Turn_emphasized_mode_onoff(00);
            serial.EnviaDados(bytestosend);
        }
        #endregion


        #region Comando de impressão
        // AUTHOR : MARCO AURELIO HAHN DOS SANTOS
        // COMANDOS PARA BK-T680
        //Imprimir uma linha de feed

        //=============================================//
        // Saltar uma linha                            // 
        //=============================================//
        public virtual byte[] Print_feed_one_line()
        {
            byte[] Retorno = { 0X0A };
            return Retorno;
        }

        //=============================================//
        // Limpar Buffer Imagem                              // 
        //=============================================//
        public virtual byte[] Location_Image()
        {
            byte[] Retorno = { 0X1B, 0X40 };

            return Retorno;
        }

        //=============================================//
        // Limpar Buffer                               // 
        //=============================================//
        public virtual byte[] Clear_Buffer()
        {
            byte[] Retorno = { 0X0C };
            return Retorno;
        }
       
        //===========================================//
        //     Habilita Status Automatico            //
        //===========================================//
        public virtual byte[] Automatic_Back_Status_ON()
        {
            byte[] Retorno = { 0X1D, 0X61, 0X01 };
            return Retorno;
        }

        //Imprima e alimente papel.
        public virtual byte[] Print_feed_paper()
        {
            byte[] Retorno = { 0X0C };
            return Retorno;
        }

        public virtual byte[] Define_Number_ImageDownload()
        {
            byte[] Retorno = { 0X1D, 0X23, 0X00 };
            return Retorno;
        }
        public virtual byte[] Define_DoenloadBitImage()
        {
            byte[] Retorno = { 0X1D, 0X2A, 0X1C, 0X08 };
            return Retorno;
        }
        public virtual byte[] Print_Raw_Image()
        {
            byte[] Retorno = { 0X1D, 0X2F, 0X00 };
            return Retorno;
        }
        //Impressão e retorno de carro.
        public virtual byte[] Print_carriage_return()
        {
            byte[] Retorno = { 0X0D };
            return Retorno;
        }

        //Imprimir no modo de página.
        public virtual byte[] Print_page_mode()
        {
            byte[] Retorno = { 0X1B, 0X0C };
            return Retorno;
        }

        //Imprimir e alimentar papel n linhas.
        //Imprima os dados na área do buffer e alimente o papel[n × unidade 
        //de movimento vertical ou horizontal] polegadas.
        public virtual byte[] Print_feed_paper_n_lines(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X4A, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Inicializar impressora
        public virtual byte[] Print_initialize()
        {
            byte[] Retorno = { 0X1B, 0X40 };
            return Retorno;
        }

        //Imprimir Resolucao 203x203.
        public virtual byte[] Print_resultion_203x203()
        {
            byte[] Retorno = { 0X1B, 0X40, 0X1D, 0X50, 0XCB, 0XCB };
            return Retorno;
        }

        public virtual byte[] switch_standard_mode()
        {
            byte[] Retorno = { 0X1B, 0X53 };
            return Retorno;
        }

        //Imprimir Resolucao 80x203.
        public virtual byte[] Print_resultion_80x203()
        {
            byte[] Retorno = { 0X1B, 0X40, 0X1B, 0X4A, 0X50 };
            return Retorno;
        }

        // Imprima e alimente n linhas
        public virtual byte[] Print_feed_n_lines(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X64, byte.Parse(n.ToString()) };
            return Retorno;
        }
        #endregion


        #region Comando de localização

        //  Move a posição de impressão para a próxima posição de tabulação horizontal.
        public virtual byte[] Horizontal_tabs()
        {
            byte[] Retorno = { 0X09 };
            return Retorno;
        }

        //entrar no modo padrão
        public virtual byte[] horizontal_tab_positione()
        {
            byte[] Retorno = { 0X1B, 0X44, 0X08, 0X10, 0X1C, 0X00 };
            return Retorno;
        }

        ////Definir a posição de impressão absoluta
        //public byte[] Set_absolute_print_position()
        //{
        //    byte[] Retorno = { 0X1B };
        //    return Retorno;
        //}

        //Selecione a direção de impressão no modo de página
        public virtual byte[] Set_print_area_page_mode(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X57, byte.Parse(n.ToString()) };
            return Retorno;
        }


        //Definir área de impressão no modo de página
        public virtual byte[] Select_print_direction_page_mode(Int32 xL,
                                                               Int32 xH,
                                                               Int32 yL,
                                                               Int32 dxL,
                                                               Int32 dxH,
                                                               Int32 dyL,
                                                               Int32 dyH)
        {
            xL = 0x0 + xL;
            xH = 0x0 + xH;
            yL = 0x0 + yL;
            dxL = 0x0 + dxL;
            dxH = 0x0 + dxH;
            dyL = 0x0 + dyL;
            dyH = 0x0 + dyH;


            byte[] Retorno = { 0X27,
                0X84,
                byte.Parse(yL.ToString()),
                byte.Parse(xH.ToString()),
                byte.Parse(yL.ToString()),
                byte.Parse(dxL.ToString()),
                byte.Parse(dxH.ToString()),
                byte.Parse(dyL.ToString()),
                byte.Parse(dyH.ToString())};
            return Retorno;
        }

        //Definir a posição de impressão relativa horizontal.
        public virtual byte[] Set_horizontal_relative_print_position(Int32 nL, Int32 nH)
        {
            nL = 0x0 + nL;
            nH = 0x0 + nH;

            byte[] Retorno = { 0X1B, 0X5C, byte.Parse(nL.ToString()), byte.Parse(nH.ToString()) };
            return Retorno;
        }

        //Selecione o modo de alinhamento de caracteres..
        public virtual byte[] Select_character_alignment_mode(Int32 n)
        {
            n = 0x0 + n;
            //byte[] Retorno = { 0X1B, 0X61, byte.Parse(n.ToString()) };
            byte[] Retorno = { 0X1B, 0X61, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Definir a posição de impressão vertical absoluta no modo de página.
        public virtual byte[] Set_absolute_vertical_print_positionin_page_mode(
            Int32 nL, Int32 nH)
        {
            nL = 0x0 + nL;
            nH = 0x0 + nH;
            byte[] Retorno = { 0X1D, 0X24, byte.Parse(nL.ToString()), byte.Parse(nH.ToString()) };
            return Retorno;
        }

        //Definir margem esquerda.
        public virtual byte[] Set_left_margin(Int32 nL, Int32 nH)
        {
            nL = 0x0 + nL;
            nH = 0x0 + nH;
            byte[] Retorno = { 0X1D, 0X4C, byte.Parse(nL.ToString()), byte.Parse(nH.ToString()) };
            return Retorno;
        }

        //Definir unidades de movimento horizontal e vertical
        public virtual byte[] Set_horizontal_vertical_motion_units(Int32 xL,
                                                     Int32 xH,
                                                     Int32 yL,
                                                     Int32 yH)
        {
            xL = 0x0 + xL;
            xH = 0x0 + xH;
            yL = 0x0 + yL;
            yH = 0x0 + yH;

            byte[] Retorno = { 0X1D,
                0X50,
                byte.Parse(xL.ToString()),
                byte.Parse(xH.ToString()),
                byte.Parse(yL.ToString()),
                byte.Parse(yH.ToString())};
            return Retorno;
        }

        //Definir a largura da área de impressão.
        public virtual byte[] Set_printing_width(Int32 nL, Int32 nH)
        {
            nL = 0x0 + nL;
            nH = 0x0 + nH;

            byte[] Retorno = { 0X1D,
                0X57,
                byte.Parse(nL.ToString()),
                byte.Parse(nH.ToString())};
            return Retorno;
        }

        //Defina a posição de impressão vertical relativa no modo de página.
        public virtual byte[] Set_relative_vertical_print_position_page_mode(Int32 nL, Int32 nH)
        {
            nL = 0x0 + nL;
            nH = 0x0 + nH;
            byte[] Retorno = { 0X1D,
                0X29,
                byte.Parse(nL.ToString()),
                byte.Parse(nH.ToString())};
            return Retorno;
        }

        //Definir a posição de impressão para o início da linha de impressão
        public virtual byte[] Set_print_position_beginning_print_line(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X54, byte.Parse(n.ToString()) };
            return Retorno;
        }
        #endregion
        #region Comando de Caracteres
        //Cancelar dados de impressão no modo de página
        public virtual byte[] Cancel_print_data_page_mode()
        {
            byte[] Retorno = { 0X18 };
            return Retorno;
        }
        //Definir o espaçamento do lado direito do caractere
        public virtual byte[] Set_character_right_side_spacing(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X20, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Selecione o (s) modo (s) de impressão de caracteres
        public virtual byte[] SelectCharacterPrintModes(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X21, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Selecione / cancele o conjunto de caracteres definidos pelo usuário
        public virtual byte[] Select_cancel_user_defined_character_set(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X25, byte.Parse(n.ToString()) };
            return Retorno;
        }
        
        //Ativar / desativar o modo de sublinhado
        public virtual byte[] Turn_underline_mode_onoff(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X2D
                    , byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Ativar / desativar o modo enfatizado
        public virtual byte[] Turn_emphasized_mode_onoff(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X45
                    , byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Ligue / desligue o modo de golpe duplo.
        public virtual byte[] Turn_onoff_double_strike_mode(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X47
                    , byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Selecione a fonte do caractere
        public virtual byte[] Select_character_font(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X4D
                    ,byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Selecione um conjunto de caracteres internacional
        public virtual byte[] Select_international_character_set(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X52, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Ativar / desativar o modo de rotação no sentido horário de 90 °
        public virtual byte[] Turn_90_clockwise_rotation_mode_onoff(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X56, byte.Parse(n.ToString()) };
            return Retorno;
        }
        // selecione a página de códigos
        //Description]Select a page n from the codepage table: 
        //    n Page 
        //    0 PC437 ==> USA
        //    1 Katakana ==> Katakana
        //    2 PC850 ==>  Multilingual
        //    3 PC860 ==> Português
        //    4 PC863 ==> Canadian- French
        //    5 PC865 ==> Nordic
        //    16 WPC1252 
        //    17 PC866 ==> Nordic
        //    18 PC852 ==> Latin2
        //    19 PC858 ==> 
        public virtual byte[] Select_codepage(Int32 n, Serial serial)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X74, byte.Parse(n.ToString()) };
            serial.EnviaDados(Retorno);
            return Retorno;
        }
        // Ativar / desativar o modo de impressão invertida.
        public virtual byte[] Turn_onoff_upside_down_printing_mode(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X7B, byte.Parse(n.ToString()) };
            return Retorno;
        }
        // Selecione o tamanho do caractere
        public virtual byte[] Select_character_size(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X21, byte.Parse(n.ToString()) };
            return Retorno;
        }
        // Selecione o modo de impressão reversa branco / preto
        public virtual byte[] Select_whiteblack_reverse_printing_mode(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X42, byte.Parse(n.ToString()) };
            return Retorno;
        }
        // Defina o espaçamento entre os caracteres direito e esquerdo.
        public virtual byte[] Set_right_left_character_spacing(Int32 n1, Int32 n2)
        {
            n1 = 0x0 + n1;
            n2 = 0x0 + n2;
            byte[] Retorno = { 0X1C, 0X53, byte.Parse(n1.ToString()), byte.Parse(n2.ToString()) };
            return Retorno;
        }
        #endregion
        #region Comandos BitMap
        // VERIFICAR FUNCIONAMENTO COMANDOS
        //Faça o download e imprima a imagem de bit.
        //MARCO --> VERIFICAR
        //public byte[] Download_print_bit_image(Int32 n1, Int32 n2)
        //1B 2A m nL nH d1...dk
        //{
        //    n1 = 0x0 + n1;
        //    n2 = 0x0 + n2;
        //    byte[] Retorno = { 0X1B, 0X2A, byte.Parse(n1.ToString()), byte.Parse(n2.ToString()) };
        //    return Retorno;
        //}
        //Especifique um número para a imagem de bit a ser baixada
        public virtual byte[] Numbe_bit_image_downloaded(Int32 n1)
        {
            n1 = 0x0 + n1;
            byte[] Retorno = { 0X1C, 0X53, byte.Parse(n1.ToString()) };
            return Retorno;
        }
        #endregion
        #region Status command 

        //Transmissão de status em tempo real
        public virtual byte[] Real_time_status_transmission(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X10, 0X04, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Ativar / Desativar Automatic Status Back (ASB).
        public virtual byte[] EnableDisable_Automatic_Status_Back(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X61, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Transmitir status..
        public virtual byte[] Transmit_status(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X72, byte.Parse(n.ToString()) };
            return Retorno;
        }

        #endregion
        #region Barcode command 
        //Selecione a posição de impressão para caracteres HRI.
        public virtual byte[] Select_printing_position_HRI_characters(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X48, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Selecione a fonte para caracteres HRI (Human Readable Interpretation).
        public virtual byte[] Select_font_Human_Readable_Interpretation_HRI_characters(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X48, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Selecione a altura do código de barras
        public virtual byte[] Select_barcode_height(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X68, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Selecione caractere condensado do código de barras
        public virtual byte[] Select_barcode_character_condensed()
        {
            byte[] Retorno = { 0X1D, 0X66, 0X01 };
            return Retorno;
        }



        //Selecione um tipo de código de barras e imprima o código de barras.
        //VERICAR ENVIO MHS 12/04/2019
        public virtual byte[] Barcode_Character_Above()
        {
            byte[] Retorno = { 0X1D, 0X77, 0X02 };
            return Retorno;
        }

        //Selecione um tipo de código de barras e imprima o código de barras.
        //VERICAR ENVIO MHS 12/04/2019
        /// <summary>
        /// Código de barras padrão CODE128
        /// </summary>
        /// <param name="textBarCode"></param>
        /// <returns></returns>
        public virtual byte[] Select_barcode_type_print_Normal(string textBarCode)
        {
            string ValorTextoBarCode = $"GSk{textBarCode}0A";
            byte[] Retorno = ConvertByte(ValorTextoBarCode, "CODE128");
            return Retorno;
        }


        //Selecione um tipo de código de barras e imprima o código de barras.
        //VERICAR ENVIO MHS 12/04/2019
        /// <summary>
        /// Código de barras padrão PDF147
        /// </summary>
        /// <param name="textBarCode"></param>
        /// <returns></returns>
        public virtual byte[] Select_barcode_type_print(string textBarCode)
        {
            string ValorTextoBarCode = $"GSk{textBarCode}0A";
            byte[] Retorno = ConvertByte(ValorTextoBarCode, "PDF147");
            //byte[] Retorno = { 0X1D, 0X6B, 0X0A, 0X30, 0X31, 0X32, 0X33, 0X34, 0X35, 0X36, 0X37, 0X38, 0X39, 0X30, 0X35, 0X39,0X00, 0X0A };
            return Retorno;
        }

        /// <summary>
        /// Código de barras padrão QRCode
        /// </summary>
        /// <param name="textBarCode"></param>
        /// <returns></returns>
        public virtual byte[] Select_QRcode_type_print(string textBarCode)
        {
            string ValorTextoBarCode = $"{textBarCode}0A";
            byte[] Retorno = ConvertByte(ValorTextoBarCode, "QRCode");
            return Retorno;
        }

        /// <summary>
        /// Código de barras padrão QRCode
        /// </summary>
        /// <param name="textBarCode"></param>
        /// <returns></returns>
        public virtual byte[] Select_QRcode_type_print_1(string textBarCode, string cod)
        {
            string ValorTextoBarCode = $"{textBarCode}0A";
            byte[] Retorno = ConvertBytepdf(ValorTextoBarCode, "QRCode", byte.Parse(cod));
            return Retorno;
        }

        //Definir parâ
        //Definir parâmetros do código de barras QRCODE
        public virtual byte[] Set_parameters_QRCODE_barcode(Int32 m,
                                                   Int32 nA,
                                                   Int32 nB,
                                                   Int32 nC)
        {
            m = 0x0 + 0;   //Fixado em zero, m = 0.
            nA = 0x0 + nA; //Largura do elemento básico do QRCode(tamanho), valores aceitos 2,3,4,5 e 6.
            nB = 0x0 + nB; //Language mode, 0:Chinese; 1:Japanese.
            nC = 0x0 + nC; //Symbol type, 1:Original type; 2:Enhanced type(Recommended).
            byte[] Retorno = { 0X1D, 0X6F, byte.Parse(m.ToString()),
            byte.Parse(nA.ToString()),
            byte.Parse(nB.ToString()),
            byte.Parse(nC.ToString()),
            byte.Parse("10")};
            return Retorno;
        }

        //Definir parâ
        //Definir parâmetros do código de barras QRCODE
        public virtual byte[] Set_parameters_QRCODE_barcode_1(Int32 n)
        {
            byte[] Retorno = { 0X1D, 0X28, 0x6B, 0X00, 0X31, 0X43, byte.Parse(n.ToString()), 0X0A };
            return Retorno;
        }

        //Definir grau de correção de código de barras PDF417
        //Definir grau de correção do código PDF417, quanto maior o grau de correção, 
        //maior a capacidade do código de barras
        public virtual byte[] Set_correction_grade_Barcode_PDF417(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X71, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Definir grau de correção de código de barras PDF417
        //Definir grau de correção do código PDF417, quanto maior o grau de correção, 
        //maior a capacidade do código de barras
        public virtual byte[] Set_barcode_with_PDF417(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X77, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Definir largura do código de barras
        //Defina o tamanho horizontal do código de barras.
        public virtual byte[] Set_barcode_width(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X77, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Definir a largura do módulo do DataBar GS1 bidimensional
        public virtual byte[] Set_module_width_2dimensional_GS1_DataBar(Int32 pL,
                                                   Int32 pH,
                                                   Int32 cn,
                                                   Int32 fn,
                                                   Int32 n)
        {
            pL = 0x0 + pL;
            pH = 0x0 + pH;
            cn = 0x0 + cn;
            fn = 0x0 + fn;
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X28,0X6B ,byte.Parse(pL.ToString()),
            byte.Parse(pH.ToString()),
            byte.Parse(cn.ToString()),
            byte.Parse(fn.ToString()),
            byte.Parse(n.ToString())};
            return Retorno;
        }
        //Defina a largura máxima do GS1 DataBar Expanded Stacked
        public virtual byte[] Set_maximum_width_GS1_DataBar2dimensional(Int32 pL,
                                                   Int32 pH,
                                                   Int32 cn,
                                                   Int32 fn,
                                                   Int32 nL,
                                                   Int32 nH)
        {
            pL = 0x0 + pL;
            pH = 0x0 + pH;
            cn = 0x0 + cn;
            fn = 0x0 + fn;
            nL = 0x0 + nL;
            nH = 0x0 + nH;
            byte[] Retorno = { 0X1D, 0X28, 0X6B, byte.Parse(pL.ToString()),
            byte.Parse(pH.ToString()),
            byte.Parse(cn.ToString()),
            byte.Parse(fn.ToString()),
            byte.Parse(nL.ToString()),
            byte.Parse(nH.ToString())};
            return Retorno;
        }
        ////Definir e imprimir a barra de dados GS1.
        ///VER FUNCIONALIDADE ????
        //public byte[] Define_print_GS1_Databar(Int32 pL,
        //                                           Int32 pH,
        //                                           Int32 cn,
        //                                           Int32 fn,
        //                                           Int32 nL,
        //                                           Int32 nH)
        //{
        //    pL = 0x0 + pL;
        //    pH = 0x0 + pH;
        //    cn = 0x0 + cn;
        //    fn = 0x0 + fn;
        //    nL = 0x0 + nL;
        //    nH = 0x0 + nH;
        //    byte[] Retorno = { 0X1D, 0X28, 0X6B, byte.Parse(pL.ToString()),
        //    byte.Parse(pH.ToString()),
        //    byte.Parse(cn.ToString()),
        //    byte.Parse(fn.ToString()),
        //    byte.Parse(nL.ToString()),
        //    byte.Parse(nH.ToString())};
        //    return Retorno;
        //}
        //Definir a largura do módulo da simbologia composta
        //Nota Defina uma largura de módulo de simbologia composta para n pontos. 
        //Padrão n= 2
        public virtual byte[] Set_module_width_composite_symbology(Int32 k,
                                                  Int32 pL,
                                                  Int32 pH,
                                                  Int32 cn,
                                                  Int32 fn,
                                                  Int32 n)
        {
            k = 0x0 + k;
            pL = 0x0 + pL;
            pH = 0x0 + pH;
            cn = 0x0 + cn;
            fn = 0x0 + fn;
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X28, 0X6B,byte.Parse(k.ToString()),
            byte.Parse(pL.ToString()),
            byte.Parse(pH.ToString()),
            byte.Parse(cn.ToString()),
            byte.Parse(fn.ToString()),
            byte.Parse(n.ToString())};
            return Retorno;
        }
        //Definir a largura máxima de GS1 DataBar expandida empilhada de simbologia composta
        public virtual byte[] Set_maximum_width_GS_DataBar_expanded_stacked_composite_symbology(
                                                    Int32 k,
                                                    Int32 pL,
                                                    Int32 pH,
                                                    Int32 cn,
                                                    Int32 fn,
                                                    Int32 nL,
                                                    Int32 nH)
        {
            k = 0x0 + k;
            pL = 0x0 + pL;
            pH = 0x0 + pH;
            cn = 0x0 + cn;
            fn = 0x0 + fn;
            nL = 0x0 + nL;
            nH = 0x0 + nH;
            byte[] Retorno = { 0X1D, 0X28, 0X6B,byte.Parse(k.ToString()),
            byte.Parse(pL.ToString()),
            byte.Parse(pH.ToString()),
            byte.Parse(cn.ToString()),
            byte.Parse(fn.ToString()),
            byte.Parse(nL.ToString()),
            byte.Parse(nH.ToString())};
            return Retorno;
        }
        //Definir uma fonte para o caractere HRI ao imprimir o código de barras
        //Notas : 
        //Selects whether or not to use the font of HRI character, and selects a font by n as following: n Font 
        //0,48 Does not use HRI character.
        //1,49 Standard ASCII character(12 × 24)
        //2,50 Compressed ASCII character(9 × 17)
        //97 Special standard ASCII character(12 × 24)
        //98 Special compressed ASCII character(9 × 17)
        public virtual byte[] Set_font_HRI_character_print_barcode(
                                                    Int32 pL,
                                                    Int32 pH,
                                                    Int32 cn,
                                                    Int32 fn,
                                                    Int32 n)
        {
            pL = 0x0 + pL;
            pH = 0x0 + pH;
            cn = 0x0 + cn;
            fn = 0x0 + fn;
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X28, 0X6B,
            byte.Parse(pL.ToString()),
            byte.Parse(pH.ToString()),
            byte.Parse(cn.ToString()),
            byte.Parse(fn.ToString()),
            byte.Parse(n.ToString())};
            return Retorno;
        }
        ////Definir e imprimir simbologia composta GS1.
        ///VER FUNCIONALIDADE ????
        //public byte[] Define_print_GS1_composite_ symbology(Int32 pL,
        //                                           Int32 pH,
        //                                           Int32 cn,
        //                                           Int32 fn,
        //                                           Int32 nL,
        //                                           Int32 nH)
        //{
        //    pL = 0x0 + pL;
        //    pH = 0x0 + pH;
        //    cn = 0x0 + cn;
        //    fn = 0x0 + fn;
        //    nL = 0x0 + nL;
        //    nH = 0x0 + nH;
        //    byte[] Retorno = { 0X1D, 0X28, 0X6B, byte.Parse(pL.ToString()),
        //    byte.Parse(pH.ToString()),
        //    byte.Parse(cn.ToString()),
        //    byte.Parse(fn.ToString()),
        //    byte.Parse(nL.ToString()),
        //    byte.Parse(nH.ToString())};
        //    return Retorno;
        //}
        //Entre / Saia do modo de impressão de duas cores.
        //Notas 
        //• n=0, exit two-color mode. • n= 1, enter two-color mode. 
        //Default n = 1 
        public virtual byte[] EnterExit_twocolor_print_mode(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X43, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Selecione a cor de impressão
        //Notas 
        //• n=0, select color 1.  • n=1, select color 2. 
        public virtual byte[] Select_print_color(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X72, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Comando de configuração de impressão em duas cores, definir a cor de impressão e entrar / sair do modo de duas cores.
        //Notas
        //• a=48, exit two-color print mode. 
        //• a=49, enter two-color print mode, and select color 1. 
        //• a=50, enter two-color print mode, and select color 2. 
        public virtual byte[] Twocolor_print_setting_command(
                                                    Int32 N,
                                                    Int32 pL,
                                                    Int32 pH,
                                                    Int32 fn,
                                                    Int32 a)
        {
            pL = 0x0 + pL;
            pH = 0x0 + pH;
            fn = 0x0 + fn;
            a = 0x0 + a;
            byte[] Retorno = { 0X1D, 0X28, 0X4E,
            byte.Parse(pL.ToString()),
            byte.Parse(pH.ToString()),
            byte.Parse(fn.ToString()),
            byte.Parse(a.ToString())};
            return Retorno;
        }
        //Entrar no modo de impressão invertido, comece a introduzir dados invertidos.
        //Notas 
        public virtual byte[] Enter_upsidedown_print_mode(
                                                   Int32 nL,
                                                   Int32 nH)
        {
            nL = 0x0 + nL;
            nH = 0x0 + nH;
            byte[] Retorno = { 0X1D, 0X28, 0X7A,
            byte.Parse(nL.ToString()),
            byte.Parse(nH.ToString()),
            0x30,0x53};
            return Retorno;
        }
        //Imprimir dados da página, sair do modo de impressão invertida e 
        //entrar no modo de impressão normal
        public virtual byte[] Print_page_data(Int32 nL,
                                      Int32 nH)
        {
            nL = 0x0 + nL;
            nH = 0x0 + nH;
            byte[] Retorno = { 0X1D, 0X28, 0X7A,
            byte.Parse(nL.ToString()),
            byte.Parse(nH.ToString()),
            0x30,0x45};
            return Retorno;
        }
        //Definir parâmetros de bitmap de marca d'água e inserir o modo de marca d'água.
        public virtual byte[] Set_parameters_watermark_bitmap(
                                                  Int32 n1,
                                                  Int32 n2,
                                                  Int32 n3,
                                                  Int32 n4,
                                                  Int32 n5)
        {
            n1 = 0x0 + n1;
            n2 = 0x0 + n2;
            n3 = 0x0 + n3;
            n4 = 0x0 + n4;
            n5 = 0x0 + n5;
            byte[] Retorno = { 0X1D, 0X7B, 0X77,0X02,
            byte.Parse(n1.ToString()),
            byte.Parse(n2.ToString()),
            byte.Parse(n3.ToString()),
            byte.Parse(n4.ToString()),
            byte.Parse(n5.ToString())};
            return Retorno;
        }
        //Digitar \ Sair do modo de marca d'água
        public virtual byte[] EnterExit_watermark_mode(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X7B, 0X77, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Definir parâmetros de bitmap de marca d'água, digite o modo de marca d'água.
        public virtual byte[] Set_parameters_watermark_bitmap_mode(
                                                 Int32 n1,
                                                 Int32 n2,
                                                 Int32 n3,
                                                 Int32 n4,
                                                 Int32 n5,
                                                 Int32 n6,
                                                 Int32 n7)
        {
            n1 = 0x0 + n1;
            n2 = 0x0 + n2;
            n3 = 0x0 + n3;
            n4 = 0x0 + n4;
            n5 = 0x0 + n5;
            n6 = 0x0 + n6;
            n7 = 0x0 + n7;
            byte[] Retorno = { 0X1D, 0X99, 0X42,0X45,0X92,0X9a,0X45,0X30,0X02,
            byte.Parse(n1.ToString()),
            byte.Parse(n2.ToString()),
            byte.Parse(n3.ToString()),
            byte.Parse(n4.ToString()),
            byte.Parse(n5.ToString()),
            byte.Parse(n6.ToString()),
            byte.Parse(n7.ToString())};
            return Retorno;
        }
        //Ativar / Desativar o modo de impressão de marca d'água do lado reverso / reverso
        //Notas
        // n1 indica impressão de marca d'água: 
        //n1 = 0: indica que a impressão de marca d'água está desativada 
        //n1 = 1: indica que a impressão de marca d'água está ativada  
        //n2 indica o número da marca d'água. 
        //(Nota: Esse número de marca d'água deve corresponder ao número de marca d'água definido.) 
        // n3 indica o número lateral         
        //n3 = 0: indica para imprimir no lado anverso.
        //n3 = 1: indica para imprimir no verso. 
        // Este comando é ativado somente quando processado no início da linha. 
        // Antes de usar este comando, defina primeiro os parâmetros de marca d'água 
        //usando o comando de configuração de marca d'água. 
        // A impressora retorna ao modo de impressão normal após usar este comando 
        //para sair do modo de marca d'água. [modo de impressão de marca d'água lateral
        public virtual byte[] EnableDisable_obverse_sidereverse(
                                         Int32 n1,
                                         Int32 n2,
                                         Int32 n3)
        {
            n1 = 0x0 + n1;
            n2 = 0x0 + n2;
            n3 = 0x0 + n3;
            byte[] Retorno = { 0X1D, 0X99, 0X42,0X45,0X92,0X9a,0X45,0X30,
            byte.Parse(n1.ToString()),
            byte.Parse(n2.ToString()),
            byte.Parse(n3.ToString())};
            return Retorno;
        }
        //Configurar os parâmetros de bitmap e insira o modo de impressão de aninhamento
        //Notas
        //n1 indica o modo de impressão de aninhamento:
        //Manual de programação BK-T680 / BK-T6112
        //- 74 -
        //n1 = 0: justificação à esquerda 
        //n1 = 1: centralização 
        //n1 = 2: justificação à direita  
        //n2 indica o modo invertido: n2 = 0: o modo invertido está desativado 
        //n2 = 1: o modo invertido está ativado  
        //n3 indica o modo de ampliação seleção de impressão aninhada. 
        //0-3 bits selecionam altura
        public virtual byte[] Set_bitmap_parameters(
                                                Int32 n1,
                                                Int32 n2,
                                                Int32 n3,
                                                Int32 n4,
                                                Int32 n5
                                           )
        {
            n1 = 0x0 + n1;
            n2 = 0x0 + n2;
            n3 = 0x0 + n3;
            n4 = 0x0 + n4;
            n5 = 0x0 + n5;
            byte[] Retorno = { 0X1D, 0X99, 0X42,0X45,0X92,0X9a,0X45,0X31,0X02,
            byte.Parse(n1.ToString()),
            byte.Parse(n2.ToString()),
            byte.Parse(n3.ToString()),
            byte.Parse(n4.ToString()),
            byte.Parse(n5.ToString())};
            return Retorno;
        }
        //Ativar/desativar o modo de impressão de aninhamento lateral anverso / verso
        //Notas        
        //n1 indica impressão aninhada:
        //n1 = 0: a impressão aninhada está desabilitada.
        //n1 = 1: a impressão aninhada está ativada. 
        // n2 indica o número de impressão do aninhamento. 
        // n3 indica o número lateral.
        //n3 = 0: aninhamento de impressão no lado anverso.
        //n3 = 1: aninhamento de impressão no verso. 
        // Este comando é ativado somente quando processado no início da linha.
        // Antes de usar este comando, defina primeiro os parâmetros de impressão aninhada 
        //usando o comando de configuração de impressão aninhada. 
        // A impressora retorna ao modo de impressão normal depois de usar este comando 
        //para sair do modo de impressão aninhada.
        public virtual byte[] EnableDisable_obverse_sidereverse_side_nesting_print_mode(
                                        Int32 n1,
                                        Int32 n2,
                                        Int32 n3)
        {
            n1 = 0x0 + n1;
            n2 = 0x0 + n2;
            n3 = 0x0 + n3;
            byte[] Retorno = { 0X1D, 0X99, 0X42,0X45,0X92,0X9a,0X45,0X31,
            byte.Parse(n1.ToString()),
            byte.Parse(n2.ToString()),
            byte.Parse(n3.ToString())};
            return Retorno;
        }
        
        //Imprima o bitmap de escala de cinza RAM transferido e defina o modo de impressão.
        //Notas
        //Esse comando será ignorado se o bitmap baixado não tiver sido definido. 
        // Este comando está desativado no modo de impressão invertido. 
        // Se o bitmap transferido exceder a área de impressão, os dados em excesso 
        //não serão impressos.
        public virtual byte[] Print_downloaded_RAM_gray_scale_bitmap_set_print_mode(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X63, 0X37, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Escolha o modo de economia de papel e reduza a largura do ticket.
        //Notas
        //A função de economia de papel refere-se à compressão vertical de acordo com um 
        //fator de proporcionalidade definido pelo usuário para atingir a meta de 
        //economizar papel.  O comando refere-se apenas à compressão vertical. 
        // O comando só funciona em espaço compactável.
        //O espaço compressível inclui: espaço entre os dados de 
        //impressão(exceto o espaço causado por caracteres de espaço); 
        //Código de barras 1D (O código de barras 1D de altura mínima pode ser comprimido 
        //é de 30 pontos). 
        // O comando comprime o espaço compressível de acordo com um determinado 
        //fator de proporcionalidade, que é definido da seguinte forma: m 
        //Configuração do fator de proporcionalidade
        //0 sem compressão
        //1 comprimir 25%
        //2 comprimir 50%
        //3 comprimir 75%
        //4 comprimir 100%
        // O comando só funciona no ticket enviando este comando. 
        // Este comando é ativado apenas no modo padrão.
        public virtual byte[] Choose_pape_saving_mode_reduce_ticket_width(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X63, 0X3A, byte.Parse(n.ToString()) };
            return Retorno;
        }
        public virtual byte[] Select_Number_Bit_image()
        {
            byte[] Retorno = { 0X1D, 0X23, 0X00 };
            return Retorno;
        }
        public virtual byte[] Print_Ram_Bit_image()
        {
            byte[] Retorno = { 0X1D, 0X2F, 0X00 };
            return Retorno;
        }
        public virtual byte[] Print_Clear_Buffer()
        {
            byte[] Retorno = { 0X1B, 0X0C };
            return Retorno;
        }

        /// <summary>
        /// Converte para um array byte com comandos em HEX
        /// </summary>
        /// <param name="Buffer"></param>
        /// <param name="Tipo">PDF147 CODE128 QRCode</param>
        /// <returns></returns>
        public virtual byte[] ConvertByte(string Buffer, string Tipo)
        {
            try
            {
                int i = 0;
                byte[] bufEntrada = new byte[Buffer.Length];

                bufEntrada[0] = 0X1D;
                bufEntrada[1] = 0X6B;

                switch (Tipo)
                {
                    case "PDF147":
                        bufEntrada[2] = 0X0A;
                        i = 3;
                        break;
                    case "CODE128":
                        bufEntrada[2] = 0X04;
                        i = 3;
                        break;
                    case "QRCode":
                        bufEntrada[2] = 0X0B;
                        bufEntrada[3] = 0X48;
                        i = 4;
                        break;
                }

                while (i < Buffer.Length)
                {
                    if (Buffer.Substring(i, 2) == "0A")
                    {
                        bufEntrada[i] = 0X00;
                        i++;
                        bufEntrada[i] = 0X0A;
                        break;
                    }
                    bufEntrada[i] = byte.Parse(ASCIITOHex(Buffer.Substring(i, 1)),
                        NumberStyles.HexNumber);
                    i++;
                }
                return bufEntrada;
            }
            catch (Exception ERROR)
            {
                string Erro = ERROR.Message;
                byte[] bufEntrada = new byte[0];
                return bufEntrada;
            }
        }

        /// <summary>
        /// Converte para um array byte com comandos em HEX
        /// </summary>
        /// <param name="Buffer"></param>
        /// <param name="Tipo">PDF147 CODE128 QRCode</param>
        /// <returns></returns>
        public virtual byte[] ConvertBytepdf(string Buffer, string Tipo, Int32 n)
        {
            try
            {
                int i = 0;
                byte[] bufEntrada = new byte[Buffer.Length];
                n = 0x0 + n;
                bufEntrada[0] = 0X1D;
                bufEntrada[1] = 0X6B;

                switch (Tipo)
                {
                    case "PDF147":
                        bufEntrada[2] = 0X0A;
                        i = 3;
                        break;
                    case "CODE128":
                        bufEntrada[2] = 0X04;
                        i = 3;
                        break;
                    case "QRCode":
                        bufEntrada[2] = 0X0B;
                        bufEntrada[3] = byte.Parse(n.ToString());
                        i = 4;
                        break;
                }

                while (i < Buffer.Length)
                {
                    if (Buffer.Substring(i, 2) == "0A")
                    {
                        bufEntrada[i] = 0X00;
                        i++;
                        bufEntrada[i] = 0X0A;
                        break;
                    }
                    bufEntrada[i] = byte.Parse(ASCIITOHex(Buffer.Substring(i, 1)),
                        NumberStyles.HexNumber);
                    i++;
                }
                return bufEntrada;
            }
            catch (Exception ERROR)
            {
                string Erro = ERROR.Message;
                byte[] bufEntrada = new byte[0];
                return bufEntrada;
            }
        }

        public virtual void ConvertByteEnviaTexto(string Buffer, string Tipo, string Com, Serial serial)
        {
            try
            {
                int i = 0;
                byte[] bufEntrada = new byte[Buffer.Length];

                bufEntrada[0] = 0X1D;
                bufEntrada[1] = 0X6B;

                switch (Tipo)
                {
                    case "PDF147":
                        bufEntrada[2] = 0X0A;
                        i = 3;
                        break;
                    case "CODE128":
                        bufEntrada[2] = 0X04;
                        i = 3;
                        break;
                    case "QRCode":
                        bufEntrada[2] = 0X0B;
                        bufEntrada[3] = 0X46;
                        i = 4;
                        break;
                }

                while (i < Buffer.Length)
                {
                    if (Buffer.Substring(i, 2) == "0A")
                    {
                        bufEntrada[i] = 0X00;
                        i++;
                        bufEntrada[i] = 0X0A;
                        break;
                    }
                    bufEntrada[i] = byte.Parse(ASCIITOHex(Buffer.Substring(i, 1)),
                        NumberStyles.HexNumber);
                    i++;
                }
                Select_codepage(3, serial);
                //string teste=serial.LeDadosPrinterOnOff();
                serial.EnviaDados(bufEntrada);
            }
            catch (Exception ERROR)
            {
                string Erro = ERROR.Message;
                byte[] bufEntrada = new byte[0];
            }
        }
        public virtual byte[] ConvertByteAlone(string Buffer)
        {
            try
            {
                int i = 0;
                byte[] bufEntrada = new byte[Buffer.Length + 1];

                while (i < Buffer.Length)
                {
                    bufEntrada[i] = byte.Parse(ASCIITOHex(Buffer.Substring(i, 1)),
                        NumberStyles.HexNumber);
                    i++;
                }
                bufEntrada[i] = 0x00;
                return bufEntrada;
            }
            catch (Exception ERROR)
            {
                string Erro = ERROR.Message;
                byte[] bufEntrada = new byte[0];
                return bufEntrada;
            }
        }

        public virtual void ConvertByteAloneTexto(string Buffer, string Com, Serial serial)
        {
            try
            {
                int i = 0;
                byte[] bufEntrada = new byte[Buffer.Length + 1];

                while (i < Buffer.Length)
                {
                    bufEntrada[i] = byte.Parse(ASCIITOHex(Buffer.Substring(i, 1)),
                        NumberStyles.HexNumber);
                    i++;
                }
                bufEntrada[i] = 0x00;
                Select_codepage(3, serial);
                serial.EnviaDados(bufEntrada);
            }
            catch (Exception ERROR)
            {
                string Erro = ERROR.Message;
                byte[] bufEntrada = new byte[0];

            }
        }
        public virtual string ASCIITOHex(string ascii)
        {
            StringBuilder sb = new StringBuilder();            
            byte[] inputbytes = Encoding.UTF8.GetBytes(ascii);
            foreach (byte b in inputbytes)
            {
                sb.Append(string.Format("{0:x2}", b));
            }
            return sb.ToString();
        }
        #endregion
        #region Comando relevante do lado duplo
        //==========================================================================================
        //Função do comando relevante do lado duplo: 
        //A impressora pode ser configurada para os quatro modos a seguir: 
        //1) Modo de impressão no lado único; 
        //2) modo de impressão do lado duplo com o comando do lado único.
        //Quando a impressora receber o comando de corte, ela dividirá automaticamente os dados 
        //de impressão em duas partes, a primeira parte será impressa no lado anverso e a 
        //outra parte será impressa no verso. 
        //3) Modo de impressão do lado duplo com comando do lado duplo.
        //Este comando especificará os dados de impressão a serem impressos no lado anverso 
        //ou no verso.A impressora começará a imprimir após receber o comando de corte ou iniciar 
        //o comando de impressão. 4) Modo de impressão do lado duplo com dados pré-definidos 
        //no comando do verso.A impressora imprime os dados recebidos em tempo real no lado 
        //anverso e imprime os dados pré-definidos no verso.A impressora começa a imprimir 
        //depois de receber o comando de corte.NOS
        //==========================================================================================]

        //Selecione o modo de impressão no lado único ou no lado duplo
        //Notas
        //O significado de n é o seguinte: 
        //N Modo de impressão 0 Modo de impressão no lado único, imprimindo no lado anverso. 
        //1 Modo de impressão no lado duplo com comando do lado único. 
        //2 Modo de impressão do lado duplo com comando do lado duplo. 
        //3 Modo de impressão no lado duplo com dados pré-definidos. 
        //6 Modo de impressão no lado único, imprimindo no verso.
        // Se o modo de impressão em dois lados estiver selecionado, 
        //o conteúdo da impressão poderá ser impresso no lado anverso ou no verso. 
        // Quando n = 1, 
        //o conteúdo da impressão será dividido automaticamente em duas partes, 
        //uma parte será impressa no lado anverso e a outra parte será impressa no verso.
        //O comando 1F 61 (escolha do lado reverso / reverso), 
        //1F 62 (impressão do lado duplo) é ignorado. 
        // Quando n = 2, o conteúdo da impressão será selecionado para imprimir no 
        //lado anverso ou no verso através do comando 1F 61; 
        //a impressão pode ser iniciada através do comando de envio 1F 62. 
        // Quando n = 3, os dados recebidos serão impressos no lado anverso e os 
        //dados pré-definidos serão impressos no verso; os dados pré-definidos 
        //impressos no lado do rio serão impressos apenas uma vez em cada 
        //ticket(o comando de corte é o comando de término do ticket); 
        //o comprimento do ticket é o comprimento do lado maior.
        public virtual byte[] Select_single_side_double_side_printing_mode(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1F, 0X60, byte.Parse(n.ToString()) };
            return Retorno;
        }
        //Selecione o lado de impressão térmica (lado anverso / lado reverso).
        //Notas
        //O significado de n é o seguinte: n Modo de impressão
        //0 Selecione o lado anverso.
        //1 Selecione o lado reverso.
        // Este comando só é ativado no modo de impressão de dois lados com o comando 
        //do lado duplo. 
        // Este comando só é ativado quando processado no início da linha. 
        // Se um dos lados for maior que o tamanho do buffer, 
        //a impressora imprimirá as partes excedentes.
        public virtual byte[] Select_thermal_printing_side(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1F, 0X61, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Inicie a impressão no lado duplo
        //Notas
        //Este comando só é ativado no modo de impressão de dois lados com o comando do lado duplo, 
        //ou será ignorado.
        public virtual byte[] Start_doubleside_printing()
        {
            byte[] Retorno = { 0X1F, 0X62 };
            return Retorno;
        }

        //Selecione ou cancele a impressão invertida no modo de dois lados.
        //Notas
        //O significado de n é o seguinte: Bit n Modo de impressão bit0
        //0 Cancele a impressão invertida no lado anverso. 
        //1 Selecione impressão invertida no lado anverso.
        //Bit1 0 Cancele a impressão invertida no verso. 
        //1 Selecione impressão invertida no lado anverso.
        // Esse comando só é ativado no modo de impressão de dois lados. 
        // As últimas configurações antes da impressão são efetivas. [
        public virtual byte[] Select_cancel_upsidedown_printing_under(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1F, 0X63, byte.Parse(n.ToString()) };
            return Retorno;
        }


        ////  Faça o download da mensagem de cima / baixo de uma linha / 
        /////duas linhas para a memória FLASH.
        ///VER FUNCIONALIDADE ????
        //public byte[] Download_one_line_two_line_ top_bottom_message_FLASH_memory(Int32 pL,
        //                                           Int32 pH,
        //                                           Int32 cn,
        //                                           Int32 fn,
        //                                           Int32 nL,
        //                                           Int32 nH)
        //{
        //    pL = 0x0 + pL;
        //    pH = 0x0 + pH;
        //    cn = 0x0 + cn;
        //    fn = 0x0 + fn;
        //    nL = 0x0 + nL;
        //    nH = 0x0 + nH;
        //    byte[] Retorno = { 0X1D, 0X28, 0X6B, byte.Parse(pL.ToString()),
        //    byte.Parse(pH.ToString()),
        //    byte.Parse(cn.ToString()),
        //    byte.Parse(fn.ToString()),
        //    byte.Parse(nL.ToString()),
        //    byte.Parse(nH.ToString())};
        //    return Retorno;
        //}

        //Ativar mensagem superior / inferior
        //Notas
        //O significado de n é o seguinte: Modo de impressão de valor de bit
        //Bit0
        //0 Desativar mensagem inferior pré-definida no lado anverso. 
        //1 Ative a mensagem inferior predefinida no lado anverso.
        //Bit1
        //0 Desativar a mensagem principal predefinida no verso. 
        //1 Ative a mensagem principal predefinida no verso.
        // Se este comando estiver ativado, a impressora adicionará automaticamente 
        //uma mensagem de uma linha / duas linhas na parte inferior / superior do lado 
        //anverso / verso.  Este comando só é ativado no modo de dois lados
        public virtual byte[] Enable_top_bottom_message(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1F, 0X66, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Selecione a enésima macro para definição / desempenho.
        //Notas
        //O tamanho do buffer de armazenamento de dados de definição de macro é 50 * 2048 bytes.
        // A impressora não detecta a validade dos dados macro definidos durante a 
        //definição da macro.  As definições de macro podem ser aninhadas, mas a profundidade 
        //de aninhamento pode ser apenas uma macro.
        public virtual byte[] Select_macro_definitionperformance(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1F, 0X67, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Selecione a enésima macro para definição / desempenho.
        //Notas
        //O tamanho do buffer de armazenamento de dados de definição de macro é 50 * 2048 bytes.
        // A impressora não detecta a validade dos dados macro definidos durante a 
        //definição da macro.  As definições de macro podem ser aninhadas, mas a profundidade 
        //de aninhamento pode ser apenas uma macro.
        public virtual byte[] Select_Text_Qrcode(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1F, 0X67, byte.Parse(n.ToString()), 0x03, 0x00, 0x02 };
            return Retorno;
        }



        //Comece ou termine os dados pré-definidos no verso.
        //Notas
        //Inicie ou finalize dados pré-definidos no verso.
        //E salve os dados pré-definidos na memória FLASH. 
        // Este comando é usado para iniciar e terminar a definição de dados 
        //pré-definidos no verso.  Os dados predefinidos no verso são apagados 
        //quando não há dados definidos (ou seja, há dois contínuos 1F 68).
        // A impressora não detectará a validade dos dados durante a definição de dados 
        //pré-definidos no verso. [

        public virtual byte[] Start_end_predefined_data_reverse_side()
        {
            byte[] Retorno = { 0X1F, 0X68 };
            return Retorno;
        }

        // Defina o comprimento mínimo para mudar automaticamente para o lado reverso.
        //Notas
        // Este comando só é ativado no modo de impressão de dois lados com comando de lado único.
        //No modo de impressão no lado duplo com o comando de lado único, a impressora alternará 
        //o conteúdo da impressão para o verso, se o conteúdo da impressão exceder um determinado 
        //comprimento. 
        // A unidade de altura é ponto, altura = n1 + n2 * 256(ponto). 
        // Se a impressão da altura do conteúdo exceder a altura de impressão, a impressora 
        //alterará a próxima linha de caracteres para o verso.Por exemplo, se a altura do 
        //caractere na linha 7 exceder a altura de impressão durante a linha de impressão 7, 
        //a impressora não dividirá os caracteres na linha 7, mas alternará os caracteres no 
        //início da linha 8 para o verso; mas se a impressora não puder encontrar a linha de 
        //caracteres para alternar no intervalo de 204 (ponto) para cima ou para baixo no cálculo 
        //da nova linha, a impressora dividirá o caractere de forma convincente(o conteúdo da 
        //impressão pode ser dividido). 
        public virtual byte[] Define_minimum_length_automatically_switching_reverse_side(
                                                                    Int32 n1,
                                                                    Int32 n2)
        {
            n1 = 0x0 + n1;
            n2 = 0x0 + n2;
            byte[] Retorno = { 0X1F, 0X67, byte.Parse(n1.ToString()), byte.Parse(n1.ToString()) };
            return Retorno;
        }

        //Imprime cadeias de caracteres variáveis.
        ///VER FUNCIONALIDADE ????
        //public byte[] Print_variable_ character_strings(Int32 pL,
        //                                           Int32 pH,
        //                                           Int32 cn,
        //                                           Int32 fn,
        //                                           Int32 nL,
        //                                           Int32 nH)
        //{
        //    pL = 0x0 + pL;
        //    pH = 0x0 + pH;
        //    cn = 0x0 + cn;
        //    fn = 0x0 + fn;
        //    nL = 0x0 + nL;
        //    nH = 0x0 + nH;
        //    byte[] Retorno = { 0X1D, 0X28, 0X6B, byte.Parse(pL.ToString()),
        //    byte.Parse(pH.ToString()),
        //    byte.Parse(cn.ToString()),
        //    byte.Parse(fn.ToString()),
        //    byte.Parse(nL.ToString()),
        //    byte.Parse(nH.ToString())};
        //    return Retorno;
        //}

        //Definir seqüências de caracteres variáveis.
        ///VER FUNCIONALIDADE ????
        //public byte[] Define_variable_character_strings(Int32 pL,
        //                                           Int32 pH,
        //                                           Int32 cn,
        //                                           Int32 fn,
        //                                           Int32 nL,
        //                                           Int32 nH)
        //{
        //    pL = 0x0 + pL;
        //    pH = 0x0 + pH;
        //    cn = 0x0 + cn;
        //    fn = 0x0 + fn;
        //    nL = 0x0 + nL;
        //    nH = 0x0 + nH;
        //    byte[] Retorno = { 0X1D, 0X28, 0X6B, byte.Parse(pL.ToString()),
        //    byte.Parse(pH.ToString()),
        //    byte.Parse(cn.ToString()),
        //    byte.Parse(fn.ToString()),
        //    byte.Parse(nL.ToString()),
        //    byte.Parse(nH.ToString())};
        //    return Retorno;
        //}

        //Volte para o modo de impressão atual.
        //Notas
        //Definição do modo de impressão: Bit Bit Valor Decimal Function
        //1,0
        //00 0 Modo de impressão no lado único.
        //01 1 Modo de impressão do lado duplo com comando do lado único.
        //10 2 Modo de impressão do lado duplo com comando do lado duplo.
        //11 3
        //Modo de impressão no lado duplo com comando pré-definido no verso.
        //2 0 0 Undefined, fixed to 0.
        //30 0 Select obverse side(only valid under double - side printing mode with double - side command)
        //1 8Select reverse side(only valid under double - side printing mode with double - side command)
        //4, 5
        //00 0 The paper detection is not finished. 
        //01 16 The paper detection result matches the set 2ST mode.
        //10 32
        //The paper detection result does not match the set 2ST mode. 11 48 Undefined.
        //6 0 0 Undefined, fixed to 0.
        //7 0 0 Undefined, fixed to 0.
        //  Este comando não é um comando em tempo real, a impressora executará este 
        //comando somente depois de executar todos os comandos anteriores.

        public virtual byte[] Return_back_current_print_mode(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1F, 0X6C, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Definir o tempo de espera do PRST
        //Notas
        //Ações aqui significam os modos de ação PRST especificados no comando ESC c 8. 
        //Este comando é ativado somente no modo Apresentador 1 e não é descartado papel 
        //nem modo de espera.
        public virtual byte[] Set_PRST_waitin_time(Int32 t)
        {
            t = 0x0 + t;
            byte[] Retorno = { 0X1B, 0X63, 0X39, byte.Parse(t.ToString()) };
            return Retorno;
        }

        //Definir o modo de papel grosso
        //Notas
        //O modo de papel grosso é recomendado quando o peso do papel utilizado excede 90 g; 
        //Caso contrário, o papel pode não ser ejetado normalmente. 
        // A impressora ejetará o papel diretamente se m for muito grande. 
        // A função recuperará a configuração padrão de inicialização se enviar o comando 1b 40 
        //após a alteração da EEPROM
        public virtual byte[] Set_thick_paper_mode(Int32 m)
        {
            m = 0x0 + m;
            byte[] Retorno = { 0X1B, 0X63, 0X3d, byte.Parse(m.ToString()) };
            return Retorno;
        }

        // PRESENTER retrair / descartar papel imediatamente
        //Notas
        //Este comando não altera o tempo de espera do PRESENTER.
        public virtual byte[] PRESENTER_retract_throw_paper_immediately()
        {
            byte[] Retorno = { 0X1B, 0X63, 0X49 };
            return Retorno;
        }

        // 
        //Control Presenter(efetivo no modo de controle do Presenter 2).
        /////VER FUNCIONALIDADE ????
        //public byte[] Control_Presenter_effective_Presenter_control_mode2()
        //{
        //    byte[] Retorno = { 0X1B, 0X63, 0X49 };
        //    return Retorno;
        //}

        //Selecione o espaçamento de linha padrão
        //Notas
        // O espaçamento entre linhas pode ser definido independentemente no modo padrão e no modo de página. 
        public virtual byte[] Select_default_line_spacing()
        {
            byte[] Retorno = { 0X1B, 0X32 };
            return Retorno;
        }

        //Definir o espaçamento entre linhas
        //Notas
        // O espaçamento entre linhas pode ser definido independentemente no modo padrão e 
        //no modo de página.  As unidades de movimento horizontal e vertical são especificadas 
        //por GS P. A alteração da unidade de movimento horizontal ou vertical não afeta o
        //espaçamento de linha atual. 
        // No modo padrão, a unidade de movimento vertical (y) é usada. 
        // No modo de página, este comando funciona da seguinte forma, dependendo da posição 
        //inicial e direção da área imprimível: 1) Quando a posição inicial é definida 
        //para o canto superior esquerdo ou inferior direito da área imprimível com ESC T, 
        //a unidade de movimento vertical (y) é usado. 2) 
        //Quando a posição inicial é definida no canto superior direito ou inferior esquerdo da 
        //área de impressão com ESC T, a unidade de movimento horizontal (x) é usada. 
        //A quantidade máxima de alimentação de papel é de 600 mm (23,6 polegadas) para 
        //impressoras de 300 DPI e 900 mm (35,4 polegadas) para impressoras de 203 DPI. 
        //Se uma quantidade de alimentação de papel de mais de 600 mm (300DPI) ou 900 mm (203DPI) 
        //for definida, ela será convertida para o máximo automaticamente. .
        public virtual byte[] Set_line_spacing(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X33, byte.Parse(n.ToString()) };
            return Retorno;
        }

        public virtual byte[] Clear_Buffer_Imagem(Serial serial)
        {
            byte[] Retorno = { 0X18, 0X0A, 0X0C };

            serial.EnviaDados(Retorno);
            return Retorno;
        }
        //Desativar ou ativar o dispositivo periférico
        //Notas
        // O bit mais baixo de n é efetivo;  Quando o LSB de n é 0, a impressora está desativada; 
        // Quando o LSB de n é 1, a impressora é ativada 
        // Quando a impressora é ativada quando é ligada Quando a impressora é desativada, 
        //ela ignora todos os dados, exceto o comando em tempo real (DLE EOT), 
        //até que seja ativado por esse comando.
        public virtual byte[] Disable_enable_peripheral_device(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X3D, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Inicializar impressora
        //Notas
        // Os dados de imagem de bit ou caracteres definidos pelo usuário na RAM são apagados.
        // A definição macro não está desmarcada. 
        // Os dados da imagem de bit NV não são apagados
        public virtual byte[] Initialize_printer()
        {
            byte[] Retorno = { 0X1B, 0X40, 0XA };
            return Retorno;
        }

        // Selecione o modo de página.
        // Notas
        // Este comando é ativado somente quando processado no início de uma linha no modo padrão.
        //  Este comando não tem efeito no modo de página.
        //  Depois que a impressão por FF for concluída ou por ESC S, a impressora retornará ao 
        // modo padrão. 
        //  Este comando define a posição onde os dados são armazenados em buffer para a posição 
        // especificada por ESC T dentro da área de impressão definida pelo ESC W.  Este comando 
        // alterna as configurações dos seguintes comandos para o modo de página: 1) espaçamento 
        // entre caracteres: ESC SP 2) Selecione o espaçamento entre linhas: ESC 2, ESC 3 
        //  Somente configurações de valor são possíveis para os seguintes comandos no modo de 
        // página; estes comandos não são executados até mudar para o modo padrão. 
        // 1) Ativar / desativar o modo de rotação no sentido horário de 90 °: ESC V 2) 
        // Ativar / desativar o modo de impressão invertida: ESC {3) Definir margem esquerda: GS L 4)
        // Definir a largura da área de impressão: GS W  modo padrão, após a reinicialização, 
        // ou executa ESC @
        public virtual byte[] Select_page_mode()
        {
            byte[] Retorno = { 0X1B, 0X4C };
            return Retorno;
        }

        // Selecione o modo padrão
        // Notas
        // Este comando é ativado apenas no modo de página. 
        //  Os dados armazenados em buffer no modo de página são apagados.
        // Este comando define a posição de impressão para o início da linha.
        //  A zona do modo de página é inicializada como padrão.
        //  Este comando muda as configurações dos seguintes comandos para os do modo padrão: 1) 
        // Defina o espaçamento dos caracteres do lado direito: ESC SP 2) ESC 2, ESC 3 
        //  Os comandos a seguir são ativados somente para serem configurados no modo pagemode. 1) 
        // ESC W 2) Selecione a direção de impressão no modo de página: ESC T 
        //  Os seguintes comandos são ignorados no modo padrão. 1) Defina a posição 
        // de impressão vertical absoluta no modo de página: GS $ 2) Defina a posição 
        // de impressão vertical relativa no modo de página: GS \ 
        //  O modo padrão é selecionado automaticamente quando a impressora é 
        // reinicializada ou o comando ESC @ é usado. 
        public virtual byte[] Select_standard_mode()
        {
            byte[] Retorno = { 0X1B, 0X53 };
            return Retorno;
        }

        //Selecione o tipo de papel a ser usado.
        //Notas
        // Este comando não tem efeito, exceto n é igual a 0,1,2.
        public virtual byte[] Select_paper_type_used(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X3D, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Definir posição de corte
        //Notas
        // Quando a impressora detecta a marca (a marca vai para a posição do sensor de papel.) 
        // E alimenta o papel por uma certa distância (Decidido por n: para modelo 203DPI, n / 8mm; 
        // para modelo 300DPI, n / 12mm) e, em seguida, inicia corte de papel. 
        //A relação entre N e a posição de corte é definida abaixo. Se n aumentar, a posição   
        // de corte se move para trás; Se n diminuir, a posição se move para frente: n / 8 (mm) 
        // para 203DPI, n / 12 (mm) para 300DPI.  Quando nL = nH = 255, o N salvo na impressora 
        // não será perdido mesmo quando a impressora estiver desligada / ligada. 
        //  Consulte a figura a seguir, L é a distância do papel de alimentação da impressora 
        // correspondente a n.
        public virtual byte[] Set_cutting_position(Int32 nL,
                                           Int32 nH)
        {
            nL = 0x0 + nL;
            nH = 0x0 + nH;
            byte[] Retorno = { 0X1F, 0X67, byte.Parse(nL.ToString()), byte.Parse(nH.ToString()) };
            return Retorno;
        }

        // Selecione o sensor de papel para imprimir o sinal final do papel
        // Notas
        //  É possível selecionar dois sensores para os sinais de saída. Então, 
        // se algum dos sensores detectar uma extremidade do papel, o sinal final do papel 
        // será emitido.  O comando está disponível apenas com uma interface paralela e é 
        // ignorado com uma interface serial. 
        public virtual byte[] Select_paper_sensor_output_papeend_signal(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X63, 0X33, byte.Parse(n.ToString()) };
            return Retorno;
        }

        // Selecione o(s) sensor(es) de papel para parar a impressão
        // Notas
        // Apenas o LSB de n é válido.  Quando os botões do painel estão desativados,
        // nenhum deles é utilizável.  No modo macro pronto, os botões do painel estão 
        // sempre ativados.
        public virtual byte[] Select_paper_sensor_stop_printing(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X63, 0X34, byte.Parse(n.ToString()) };
            return Retorno;
        }

        // Ativar / desativar comandos em tempo real.
        // Notas
        // Apenas o LSB de n é válido.  Quando a impressora está ligada, os comandos em 
        // tempo real estão sempre ativados.  Quando ocorre um erro, os comandos em tempo real 
        // são ativados automaticamente.
        public virtual byte[] Enabledisable_real_time_commands(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1B, 0X63, 0X40, byte.Parse(n.ToString()) };
            return Retorno;
        }

        // Cortar papéis.
        // Notas
        // Se o comprimento da alimentação de papel (L1) for menor que o comprimento mínimo de página 
        // (Lm) especificado ao executar este comando, a impressora alimentará o comprimento mínimo 
        // da página e cortará papel. Caso contrário, a impressora corta o papel diretamente. 
        //  No modo de papel marcado, a posição de corte é especificada com marcas. 
        //  No modo Apresentador 2, a impressora não entra no processo do Presenter após o corte  
        // de papel; no modo Apresentador 1, a impressora entra no processo do Presenter depois de 
        // cortar o papel. ESC
        public virtual byte[] Cut_paper()
        {
            byte[] Retorno = { 0X1B, 0X69 };
            return Retorno;

        }

        //Desenhe uma linha
        /////VER FUNCIONALIDADE ????
        //public byte[] Draw_line(Int32 n)
        //{
        //    n = 0x0 + n;
        //    byte[] Retorno = { 0X1B, 0X6C, 0X40, byte.Parse(n.ToString()) };
        //    return Retorno;
        //}

        //

        // Transmitir status da impressora
        // Notas
        // O comando é em tempo não real 
        //  Transmite o status via interface serial e / ou interface USB. 
        // Se a impressora tiver interface serial e USB, os dados retornarão à interface 
        // serial e USB ao mesmo tempo.
        public virtual byte[] Transmit_printer_status()
        {
            byte[] Retorno = { 0X1B, 0X76 };
            return Retorno;
        }

        // Execute impressão de teste.
        // Notas
        // Este comando é ativado somente quando processado no início de um modo de linha padrão.
        //  Esse comando não é efetivo no modo de página  Quando esse comando é recebido durante 
        // a definição da macro, a impressora finaliza a definição da macro e começa a executar esse 
        // comando.  A impressora corta o papel no final da impressão de teste. .
        public virtual byte[] Execute_test_print(Int32 pL,
                                         Int32 pH,
                                         Int32 n,
                                         Int32 m)
        {
            pL = 0x0 + pL;
            pH = 0x0 + pH;
            n = 0x0 + n;
            m = 0x0 + m;
            byte[] Retorno = { 0X1D, 0X28, 0X41,
                               byte.Parse(pL.ToString()),
                               byte.Parse(pH.ToString()),
                               byte.Parse(n.ToString()),
                               byte.Parse(m.ToString())};
            return Retorno;
        }

        // Selecione o modo de impressão do contador
        // Notas
        // Se n ou m fora do intervalo definido, o modo de impressão definido anteriormente não 
        // é alterado.  Se n = 0, os modos não têm significado. [
        public virtual byte[] Select_counter_print_mode(Int32 n,
                                                Int32 m)
        {
            n = 0x0 + n;
            m = 0x0 + m;
            byte[] Retorno = { 0X1D, 0X43, 0X30,
                               byte.Parse(n.ToString()),
                               byte.Parse(m.ToString())};
            return Retorno;
        }

        // Selecione o modo de contagem(A).
        // Notas
        // O modo de contagem é especificado quando [aL + aH × 256] <[bL + bH × 256], 
        // n 0 e r 0.  O modo de contagem regressiva é especificado quando [aL + aH × 256]> 
        // [bL + bH × 256], n 0 e r 0.  Contagem parar quando [aL + aH × 256] = [bL + bH × 256], 
        // n = 0 ou r = 0.  Se definir o modo de contagem, o valor mínimo de o contador é 
        // [aL + aH × 256], o número máximo é [bL + bH × 256]. Se a contagem chegar a um valor 
        // que exceda o máximo, será retomada com o valor mínimo.  Se estiver configurando o modo 
        // de contagem regressiva, o valor máximo do contador é [aL + aH × 256], o número mínimo 
        // será [bL + bH × 256] .Se a contagem regressiva atingir um valor menor que o mínimo, 
        // ela será retomada com o valor máximo.  Quando este comando é executado, o contador 
        // interno que indica o número de repetição especificado por r é apagado. 
        public virtual byte[] Select_counter_print_mode_A(Int32 aL,
                                                  Int32 aH,
                                                  Int32 bL,
                                                  Int32 bH,
                                                  Int32 n,
                                                  Int32 r)
        {
            aL = 0x0 + aL;
            aH = 0x0 + aH;
            bL = 0x0 + bL;
            bH = 0x0 + bH;
            n = 0x0 + n;
            r = 0x0 + r;
            byte[] Retorno = { 0X1D, 0X43, 0X31,
                               byte.Parse(aL.ToString()),
                               byte.Parse(aH.ToString()),
                               byte.Parse(bL.ToString()),
                               byte.Parse(bH.ToString()),
                               byte.Parse(n.ToString()),
                               byte.Parse(r.ToString())};
            return Retorno;
        }

        // Definir o contador atual
        // Notas
        // O modo de contagem é especificado quando [aL + aH × 256] <[bL + bH × 256], 
        // n 0 e r 0.  O modo de contagem regressiva é especificado quando [aL + aH × 256]> 
        // [bL + bH × 256], n 0 e r 0.  Contagem parar quando [aL + aH × 256] = [bL + bH × 256], 
        // n = 0 ou r = 0.  Se definir o modo de contagem, o valor mínimo de o contador  
        // é [aL + aH × 256], o número máximo é [bL + bH × 256]. Se a contagem chegar a um valor 
        // que exceda o máximo, será retomada com o valor mínimo. 
        //  Se estiver configurando o modo de contagem regressiva, o valor máximo do contador 
        // é [aL + aH × 256], o número mínimo será [bL + bH × 256] .Se a contagem regressiva 
        // atingir um valor menor que o mínimo, ela será retomada com o valor máximo. 
        //  Quando este comando é executado, o contador interno que indica o número de 
        // repetição especificado por r é apagado. [
        public virtual byte[] Set_current_counter(Int32 nL,
                                          Int32 mH)
        {
            nL = 0x0 + nL;
            mH = 0x0 + mH;
            byte[] Retorno = { 0X1D, 0X43, 0X32,
                               byte.Parse(nL.ToString()),
                               byte.Parse(mH.ToString())};
            return Retorno;
        }

        // Selecione o modo de contagem (B).
        // Notas
        //  O modo de contagem é especificado quando: sa <sb, sn ≠ 0, sr ≠ 0. 
        //  O modo de contagem decrescente é especificado quando: sa> sb, sn ≠ 0, sr ≠ 0. 
        //  Contagem parada quando: sa = sb ou sn = 0 ou sr = 0.
        //  Quando o modo de contagem é especificado, sa é o valor mínimo do contador e sb 
        // é o valor máximo do contador.Se o valor do contador definido por sc estiver fora do 
        // intervalo de operação do contador, o valor do contador é forçado a converter para  
        // o valor mínimo.  Quando o modo de contagem regressiva é especificado, sa é o valor 
        // máximo do contador e sb é o valor mínimo do contador.Se o valor do contador definido 
        // por sc estiver fora da faixa de operação do contador, o valor do contador será forçado 
        // a converter para o valor máximo.  Os parâmetros sa a sc podem ser omitidos. Se omitido,
        // esses valores de argumento permanecem inalterados.  Os parâmetros sa a sc não devem 
        // conter caracteres, exceto de 0 a 9. 
        //  Se uma sintaxe incorreta for usada, a configuração do parâmetro correspondente 
        // não terá efeito, e os dados depois disso serão processados ​​como dados normais.
        public virtual byte[] Select_count_mode_B(Int32 sa,
                                          Int32 sb,
                                          Int32 sn,
                                          Int32 sr,
                                          Int32 sc)
        {
            sa = 0x0 + sa;
            sb = 0x0 + sb;
            sn = 0x0 + sn;
            sr = 0x0 + sr;
            sc = 0x0 + sc;
            byte[] Retorno = { 0X1D, 0X43, 0X3B,
                               byte.Parse(sa.ToString()),
                               0X3B,
                               byte.Parse(sb.ToString()),
                               0X3B,
                               byte.Parse(sn.ToString()),
                               0X3B,
                               byte.Parse(sr.ToString()),
                               0X3B,
                               byte.Parse(sc.ToString()),
                               0X3B};
            return Retorno;
        }

        // Imprima o valor do contador.
        public virtual byte[] Print_counter_value()
        {
            byte[] Retorno = { 0X1D, 0X63 };
            return Retorno;
        }


        // Transmitir configuração da impressora
        // Notas
        //  n = 68, transmite a versão de firmware da impressora Por exemplo, “FV1.030“; 
        // Hereinto: FV é a abreviação de Firmware Version e o significado das seguintes figuras 
        // “1.030” são as seguintes: “1”: quando a alteração do hardware fez o firmware não ser  
        // compatível posteriormente, este número deve ser alterado e assim o número da versão do 
        // firmware atualizado. “03”: quando as falhas forem modificadas e a função atualizada, 
        // esse número será alterado e, portanto, o número da versão do firmware será  
        // atualizado. “0”: número da ramificação do firmware, para diferentes requisitos do 
        // cliente, que são diferentes com configurações padrão, este valor será alterado. 
        // Mas, para um cliente específico, esse número será corrigido.  n = 69, transmite a 
        // versão do carregador de boot da impressora.  n = 153, Nome da impressora de 
        // transmissão da impressora.  n = 80, transmita o número de peça da impressora. 
        // O número de peça da impressora padrão será fixado em BK-T680.  n = 81, 
        // transmite o número de série da impressora. O número de série da impressora será 
        // colocado na impressora quando sair da fábrica.  n = 82, transmite a versão de 
        // hardware da impressora.  n = 83, Transmitir data de produção da impressora  
        //  Este comando só é habilitado por meio de interfaces seriais e / ou USB.  
        // Se a impressora tiver interface serial e USB, os dados retornarão à interface  
        // serial e USB ao mesmo tempo.  `Ao usar a interface USB, o formato do comando 
        // transmitido por favor consulte 3.4 Formato de retorno de dados da interface USB
        public virtual byte[] Transmit_printer_configuration(Int32 n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X49, byte.Parse(n.ToString()) };
            return Retorno;
        }

        //Selecione o modo de corte e corte o papel
        // Notas
        //  Este comando é efetivo somente processado no início de uma linha. 
        //  Quando m = 0 ou 48, se o comprimento de alimentação do papel (L1) 
        // for menor que o comprimento mínimo de página (Lm) especificado ao executar 
        // este comando, a impressora alimentará o papel (Lm-L1) por muito tempo e cortará o papel. 
        // Caso contrário, a impressora corta o papel diretamente.       
        //  Quando m = 66, a impressora alimenta o papel (n × unidade de movimento vertical) 
        // primeiro. Se o comprimento total da alimentação de papel (L2) for menor que o comprimento 
        // mínimo da página (Lm) especificado ao executar este comando, a impressora alimentará o  
        // papel (Lm-L2) por muito tempo e cortará o papel. Caso contrário, a impressora corta o 
        // papel diretamente.  As unidades de movimento horizontal e vertical são especificadas 
        // por GS P.  A unidade de movimento vertical (y) é usada para calcular o comprimento da 
        // alimentação de papel.  No modo de papel marcado, a posição de corte é definida com marca 
        // e papel cortado.
        public virtual byte[] Select_cut_mode_paper(Int32 m)
        {
            m = 0x0 + m;
            byte[] Retorno = { 0X1D, 0X56, byte.Parse(m.ToString()) };
            return Retorno;
        }

        //Executar definição de macro
        // Notas
        // • r especifica tempos de definição de macro. 
        // • t especifica o tempo de espera da execução da macro. 
        // M especifica o modo de execução da macro. • Quando o LSB de m é 0, 
        // o tempo de intervalo da macro é t × 100 ms e a macro pode ser executada r vezes.
        // • Quando o LSB de m é 1, a impressora espera por t × 100 ms e não executa a 
        // definição de macro até que o usuário pressione o botão de alimentação com o LED 
        // piscando.O processo pode continuar r vezes. • O tempo de espera é de t × 100 ms. 
        // • Se receber este comando durante a definição da macro, a impressora parará a 
        // definição da macro e a macro definida será apagada. • Se a macro é indefinida ou 
        // r é 0, o comando está desabilitado. • No processo de execução da macro (m = 1), 
        // a impressora não pode alimentar pelo botão Feed. 
        public virtual byte[] Execute_macro_definition(Int32 r,
                                               Int32 t,
                                               Int32 m)
        {
            r = 0x0 + r;
            t = 0x0 + t;
            m = 0x0 + m;
            byte[] Retorno = { 0X1D, 0X5E,
                               byte.Parse(r.ToString()),
                               byte.Parse(t.ToString()),
                               byte.Parse(m.ToString())};
            return Retorno;
        }

        // Iniciar / terminar a definição de macro.
        // Notas
        //  The printer starts macro definition after receiving this command in normal mode. 
        // It ends macro definition after receiving this command in macro definition mode. 
        //  If the printer receives GS ^ in macro definition mode, the printer will end 
        // macro definition and clear it.  Macro definition is off when powered on. 
        //  ESC @ cannot clear macro definition, so it can be included in macro definition. 
        //  The data of macro definition can be 2048 bytes. Data out of 2048 bytes will be  
        // processed as normal data.  [
        public virtual byte[] Startend_macro_definition()
        {
            byte[] Retorno = { 0X1D, 0X3A };
            return Retorno;
        }

        //Definir o tempo de flash LED.
        // Notas
        // A impressora inicia a definição de macro depois de receber este comando no modo normal.
        // Ele termina a definição de macro depois de receber este comando no modo de definição de 
        // macro. 
        //  Se a impressora receber GS ^ no modo de definição de macro, a impressora finalizará 
        // a definição de macro e a eliminará.  A definição de macro está desativada quando ligada.
        //  ESC @ não pode limpar a definição da macro, portanto ela pode ser incluída na definição 
        // macro.  Os dados da definição de macro podem ter 2048 bytes.
        // Dados de 2048 bytes serão processados ​​como dados normais.

        public virtual byte[] Set_LED_flash_time(Int32 T1L,
                                         Int32 T1H,
                                         Int32 T2L,
                                         Int32 T2H)
        {
            T1L = 0x0 + T1L;
            T1H = 0x0 + T1H;
            T2L = 0x0 + T2L;
            T2H = 0x0 + T2H;
            byte[] Retorno = { 0X1D, 0X43, 0X32,
                               byte.Parse(T1L.ToString()),
                               byte.Parse(T1H.ToString()),
                               byte.Parse(T2L.ToString()),
                               byte.Parse(T2H.ToString())};
            return Retorno;
        }
        #endregion

        public Serial Serial => serial;

        public abstract string LeDadosPrinterOnOff();       

        protected void onRetornoSerial(object sender, SerialDataReceivedEventArgs e)
        {
            VerificaRetornoSerial((SerialPort)sender);
        }

        protected abstract void VerificaRetornoSerial(SerialPort serialAtual);
    }

}
