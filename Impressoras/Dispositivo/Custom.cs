﻿using Impressoras.Extensions;
using System;
using System.Globalization;
using System.IO.Ports;

namespace Impressoras.Dispositivo
{
    public class Custom : ImprimirPassagem
    {
        public Custom(string porta, int baudRate)
        {
            serial = new Serial(porta, baudRate == 0 ? 115200 : baudRate, Parity.None, 8, 1, 130, 130, this);
            serial.SerialPort.DataReceived += new SerialDataReceivedEventHandler(onRetornoSerial);
        }
        public override string LeDadosPrinterOnOff()
        {
            try
            {
                //Limpa Presenter = Obrigatório
                Serial.EnviaDados(new byte[] { 0x1D, 0x65, 0x05 });
                Serial.EnviaDados(Clear_Buffer());
                //Ativa retorno de status automatico (ABS)
                Serial.EnviaDados(Automatic_Back_Status_ON());
                //Solicita FULL STATUS
                Serial.EnviaDados(Real_time_status_transmission(14));
                //Lê a porta e se ocorrer timeout está desligada ou inoperante(com erro)
                return ValidarRetornoErrorSerial();
            }
            catch (Exception)
            {
                Serial.EnviaDados(Clear_Buffer());
                return "0-Impressora Desligada ou Inoperante";
            }
        }

        public override byte[] Real_time_status_transmission(int n)
        {
            n = 0x0 + n;
            byte[] Retorno = { 0X1D, 0X65, byte.Parse(n.ToString()) };
            return Retorno;
        }

        public override byte[] Cut_paper()
        {
            byte[] Retorno = {
                                0X1C, 0X50,
                                //Comprimento do papel exibido
                                0X10,
                                0X01, 0X45,
                                //Tempo de espera para ejetar
                                0X03
                             };
            return Retorno;

        }

        public override byte[] Print_initialize()
        {
            return new byte[] { 0X1D, 0X65, 0x05 };
        }          

        public override byte[] Set_parameters_QRCODE_barcode(Int32 m,
                                                   Int32 nA,
                                                   Int32 nB,
                                                   Int32 nC)
        {
            m = 0x0 + 0;   //Fixado em zero, m = 0.
            nA = 0x0 + nA; //Largura do elemento básico do QRCode(tamanho), valores aceitos 2,3,4,5 e 6.
            nB = 0x0 + nB; //Language mode, 0:Chinese; 1:Japanese.
            nC = 0x0 + nC; //Symbol type, 1:Original type; 2:Enhanced type(Recommended).
            byte[] Retorno =
                {
                    0x1D, 0x28, 0x6B, 0x03, 0x00, 0x31, 0x41, 0x00,
                    0x1D, 0x28, 0x6B, 0x03, 0x00, 0x31, 0x42, byte.Parse(nA.ToString()),
                    0x1D, 0x28, 0x6B, 0x03, 0x00, 0x31, 0x43, 0x03,
                    0x1D, 0x28, 0x6B, 0x03, 0x00, 0x31, 0x45, 0x00,
                };
            return Retorno;
        }

        public override byte[] Select_QRcode_type_print(string textBarCode)
        {
            byte[] Retorno = ConvertByte(textBarCode, "QRCode");
            return Retorno;
        }

        public override byte[] ConvertByte(string Buffer, string Tipo)
        {
            try
            {
                int command = Buffer.Length + 3;

                string bytValue = string.Format("{0,0:X4}", command);

                byte low = byte.Parse(bytValue.Substring(2), NumberStyles.HexNumber);
                byte higt = byte.Parse(bytValue.Substring(0,2), NumberStyles.HexNumber);
                byte cn = 0x31;
                byte fn = 0x50;

                if (Tipo == "PDF147")
                {
                    cn = 0x30;
                    fn = 0x51;
                }

                byte[] commandInit  = new byte[] { 0x1D, 0x28, 0x6B, low, higt, cn, fn, cn };                
                byte[] commandPrint = new byte[] { 0x1D, 0x28, 0x6B, 0x03, 0x00, cn, fn, cn };
                                
                char[] printBuffer = Buffer.ToCharArray();
                int lenPrintBuffer = printBuffer.Length;
                byte[] bufEntrada = new byte[lenPrintBuffer];

                System.Buffer.BlockCopy(printBuffer, 2, bufEntrada, 0, lenPrintBuffer);

                byte[] retorno = new byte[commandInit.Length + bufEntrada.Length + commandPrint.Length];

                commandInit.CopyTo(retorno, 0);
                bufEntrada.CopyTo(retorno, 8);
                commandPrint.CopyTo(retorno, bufEntrada.Length + 8);

                return retorno;
            }
            catch (Exception ERROR)
            {
                string Erro = ERROR.Message;
                byte[] bufEntrada = new byte[0];
                return bufEntrada;
            }
        }

        public override byte[] Set_correction_grade_Barcode_PDF417(int n)
        {
            n = 0x0 + n;
            byte[] Retorno =
               {
                    0x1D, 0x28, 0x6B, 0x03, 0x00, 0x30, 0x41, byte.Parse(n.ToString()),
                    0x1D, 0x28, 0x6B, 0x03, 0x00, 0x30, 0x42, byte.Parse(n.ToString())                   
               };
            return Retorno;
        }

        public override byte[] Set_barcode_with_PDF417(int n)
        {
            n = 0x0 + n;
            byte[] Retorno =
               {
                    0x1D, 0x28, 0x6B, 0x03, 0x00, 0x30, 0x43, byte.Parse(n.ToString()),
                    0x1D, 0x28, 0x6B, 0x03, 0x00, 0x30, 0x44, byte.Parse(n.ToString()),
                    0x1D, 0x28, 0x6B, 0x03, 0x00, 0x30, 0x45, 0x30,0x02
               };
            return Retorno;
        }

        protected override void VerificaRetornoSerial(SerialPort serialAtual)
        {
            try
            {  
                ValidarRetornoErrorSerial();
            }
            catch (Exception)
            {
            }
        }
        private string ValidarRetornoErrorSerial()
        {
            //Recebe o retorno da Serial
            byte[] bytesRetorno = new byte[serial.SerialPort.BytesToRead];
            serial.SerialPort.Read(bytesRetorno, 0, bytesRetorno.Length);
            serial.RetornoSerial = string.Empty;
            byte validByte = 0X00;
            //3rd Byte = Status de Papel
            if (bytesRetorno.Length >= 2)
            {
                validByte = bytesRetorno[2];
                if (validByte.GetBit(0))
                    serial.RetornoSerial = "3-Sem Papel|";
                if (validByte.GetBit(2))
                    serial.RetornoSerial += "2-Pouco Papel|";
            }
            //4rd Byte = User Status
            if (bytesRetorno.Length >= 3)
            {
                validByte = bytesRetorno[3];
                if (validByte.GetBit(1))
                    serial.RetornoSerial += "1-Tampa Aberta|";
            }
            //5rd Byte = Status de erro
            if (bytesRetorno.Length >= 4)
            {
                validByte = bytesRetorno[4];
                if (validByte.GetBit(6))
                    serial.RetornoSerial += "4-Papel Atolado";
            }

            return serial.RetornoSerial;
        }

        public override byte[] Automatic_Back_Status_ON()
        {
            byte[] Retorno = { 0X1D, 0XE0, 0X03 };
            return Retorno;
        }
    }
}
