﻿namespace Impressoras.Dispositivo
{
    public class TestPtinter
    {
        public string TestaStatusImp(string Com, Serial serial, int? Fecha = 0)
        {
            string retorno = "";

            //=====================================================================//
            //            VERIFICA ESTADO IMPRESSORA FILA WINDOWS                  //
            //=====================================================================//
            // retorno = TestaStatusImp($"{NamePrinter}");
            //if (retorno != "")
            //{
            //    return retorno;
            //}

            //=====================================================================//
            //            VERIFICA ESTADO IMPRESSORA PELA PORTA SERIAL             //
            //=====================================================================//
            //bytestosend = comandos.Automatic_Back_Status_ON();
            //serial.EnviaDados(bytestosend);


            retorno = serial.ImprimirPassagem.LeDadosPrinterOnOff();
            if (Fecha == 1)
                serial.EncerraSerial();

            return retorno;
        }
    }
}
