﻿namespace Impressoras.Dispositivo
{
    public class ImprimirPassagemContext
    {
        private ImprimirPassagem imprimirPassagem = null;
        
        public ImprimirPassagemContext(string fabricante, string porta, int baudRate)
        {
            switch (fabricante.ToUpper())
            { 
                case "ELGIN":
                    imprimirPassagem = new Elgin(porta, baudRate);
                    break;
                case "CUSTOM":
                    imprimirPassagem = new Custom(porta, baudRate);
                    break;
            }
           
        }
        public ImprimirPassagem GetInstanceImprimirPassagem()
        {
            return imprimirPassagem;
        }
    }
}
