﻿using System.Collections;

namespace Impressoras.Dispositivo
{
    public class BitData
    {
        public BitArray Dots
        {
            get;
            set;
        }
        public int Height
        {
            get;
            set;
        }
        public int Width
        {
            get;
            set;
        }
    }
}
