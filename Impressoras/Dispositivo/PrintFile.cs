﻿using System;
using System.Collections;
using System.Drawing;
using System.IO;

namespace Impressoras.Dispositivo
{
    public static class PrintFile
    {
        static public byte[] GetLogo(string file)
        {
            BitData data = GetBitmapData(file);
            BitArray dots = data.Dots;
            byte[] width = BitConverter.GetBytes(data.Width);
            int offset = 0;
            MemoryStream stream = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(stream);
            bw.Write((char)0x1B);
            bw.Write('@');
            bw.Write((char)0x1B);
            bw.Write('3');
            bw.Write((byte)24);
            while (offset < data.Height)
            {
                bw.Write((char)0x1B);
                bw.Write('*');         // modo de imagem de bit
                bw.Write((byte)33);    // Dupla densidade de 24 pontos
                bw.Write(width[0]);  // byte baixo da largura
                bw.Write(width[1]);  // byte alto da largura
                for (int x = 0; x < data.Width; ++x)
                {
                    for (int k = 0; k < 3; ++k)
                    {
                        byte slice = 0;
                        for (int b = 0; b < 8; ++b)
                        {
                            int y = (((offset / 8) + k) * 8) + b;
                            // Calcule a localização do pixel que queremos na matriz de bits.
                            // Estará em(y* width) +x.
                            int i = (y * data.Width) + x;
                            // Se a imagem for menor que 24 pontos, bloco com zero.
                            bool v = false;
                            if (i < dots.Length)
                            {
                                v = dots[i];
                            }
                            slice |= (byte)((v ? 1 : 0) << (7 - b));
                        }
                        bw.Write(slice);
                    }
                }
                offset += 24;
                bw.Write((char)0x0A);
            }
            // Restaure o espaçamento entre linhas para o padrão de 30 pontos.
            bw.Write((char)0x1B);
            bw.Write('3');
            bw.Write((byte)30);
            bw.Flush();
            byte[] bytes = stream.ToArray();

            return bytes;
            // return logo + Encoding.Default.GetString (bytes);
        }
        static public BitData GetBitmapData(string bmpFileName)
        {
            using (var bitmap = (Bitmap)Bitmap.FromFile(bmpFileName))
            {
                var threshold = 127;
                var index = 0;
                double multiplier = 325; // isso depende do modelo da sua impressora. para Beiyang você deve usar 1000
                double scale = (double)(multiplier / (double)bitmap.Width * 1.3);
                int xheight = (int)(bitmap.Height * scale);
                int xwidth = (int)(bitmap.Width * scale);
                var dimensions = xwidth * xheight;
                var dots = new BitArray(dimensions);
                for (var y = 0; y < xheight; y++)
                {
                    for (var x = 0; x < xwidth; x++)
                    {
                        var _x = (int)(x / scale);
                        var _y = (int)(y / scale);
                        var color = bitmap.GetPixel(_x, _y);
                        var luminance = (int)(color.R * 0.3 + color.G * 0.59 + color.B * 0.11);
                        dots[index] = (luminance < threshold);
                        index++;
                    }
                }
                
                return new BitData()
                {
                    Dots = dots,
                    Height = (int)(bitmap.Height * scale),
                    Width = (int)(bitmap.Width * scale)
                };
            }
        }

    }
}
