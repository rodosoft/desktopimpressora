﻿using System.ComponentModel;

namespace Impressoras.Enum
{
    public enum TipoDesconto
    {
        [Description("Tarifa promocional")]
        Tarifa_promocional = 1,
        [Description("Idoso")]
        Idoso = 2,
        [Description("Criançca")]
        Criança = 3,
        [Description("Deficiente")]
        Deficiente = 4,
        [Description("Estudante")]
        Estudante = 5,
        [Description("Animal doméstico")]
        Animal_domestico = 6,
        [Description("Acordo coletivo")]
        Acordo_Coletivo = 7,
        [Description("profissonal em deslocamento")]
        profissonal_em_deslocamento = 8,
        [Description("profissonal da empresa")]
        profissonal_da_empresa = 9,
        [Description("Jovem")]
        Jovem = 10,
        [Description("Outros")]
        Outros = 99,
    }
}
