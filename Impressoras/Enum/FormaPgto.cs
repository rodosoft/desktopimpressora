﻿using System.ComponentModel;

namespace Impressoras.Enum
{
    public enum FormaPgto
    {
        [Description("Dinheiro")]
        Dinheiro = 1,
        [Description("Cheque")]
        Cheque = 2,
        [Description("Crédito")]
        Crédito = 3,
        [Description("Débito")]
        Débito = 4,
        [Description("Vale transporte")]
        Vale_transporte = 5,
        [Description("Outros")]
        Outros = 99
    }
}
