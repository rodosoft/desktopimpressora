﻿using System.ComponentModel;

namespace Impressoras.Enum
{
    public enum TipoComponente
    {
        [Description("Tarifa")]
        Tarifa = 1,
        [Description("Pedágio")]
        Pedágio = 2,
        [Description("Taxa de embarque")]
        Taxa_de_embarque = 3,
        [Description("Seguro")]
        Seguro = 4,
        [Description("TMR")]
        TMR = 5,
        [Description("SVI")]
        SVI = 6,
        [Description("Outros")]
        Outros = 99
    }
}
