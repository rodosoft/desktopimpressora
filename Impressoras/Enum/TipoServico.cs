﻿using System.ComponentModel;

namespace Impressoras.Enum
{
    public enum TipoServico
    {
        [Description("Convencional com sanitário")]
        Convencional_com_sanitário = 1,

        [Description("Convencional sem sanitário")]
        Convencional_sem_sanitário = 2,

        [Description("Semileito")]
        Semileito = 3,

        [Description("Leito com ar condicionado")]
        Leito_com_ar_condicionado = 4,

        [Description("Leito sem ar condicionado")]
        Leito_sem_ar_condicionado = 5,

        [Description("Executivo")]
        Executivo = 6,

        [Description("Semiurbano")]
        Semiurbano = 7,

        [Description("Longitudinal")]
        Longitudinal = 8,

        [Description("Travessia")]
        Travessia = 9


    }
}
