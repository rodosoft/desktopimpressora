﻿using System.ComponentModel;

namespace Impressoras.Enum
{
    public enum TipoDocto
        {
            [Description("RG")]
            RG = 1,
            [Description("Tit.Eleitor")]
            Titulo = 2,
            [Description("Passaporte")]
            Passaporte = 3,
            [Description("CNH")]
            CNH = 4,
            [Description("Outro")]
            Outro = 5,
        }
}
