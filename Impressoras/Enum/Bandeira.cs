﻿using System.ComponentModel;

namespace Impressoras.Enum
{
    public enum Bandeira
    {
        [Description("Visa")]
        Visa = 1,
        [Description("Master")]
        Master = 2,
        [Description("Amex")]
        Amex = 3,
        [Description("Sorocred")]
        Soro = 4,
        [Description("Elo")]
        Elo = 5,
        [Description("Diners")]
        Diners = 6,
        [Description("Outro")]
        Outro = 99,
    }
}
