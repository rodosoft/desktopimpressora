﻿using System.ComponentModel;

namespace Impressoras.Enum
{
    public enum TipoViagem
    {
        [Description("Regular")]
        Regular = 0,
        [Description("Extra")]
        Extra = 1,
    }
}
